package com.automation.dataprovider;

import com.automation.config.AppConfig;
import com.automation.database.DataSources;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class ISAACDataProvider {

    public static final String ALL_CONTRACTS = "ALL";
    public static final String ALL_CLIENTS = "CLIENT-LIST";

    @DataProvider(name = ALL_CONTRACTS)
    public static Iterator<Object[]> all_contracts(ITestContext context) {
        int clientId = Integer.parseInt(context.getCurrentXmlTest().getParameter("client"));

        String query = "SELECT current_query(), aco from client_{0}.acos";


        List<Object[]> testDataList = new ArrayList<>();
        setUsernames(Integer.toString(clientId), context);

        try (Connection connection = DataSources.getSitDatabase()) {

                String format = MessageFormat.format(query, Integer.toString(clientId));
                PreparedStatement psinsert = connection.prepareStatement(format);
                ResultSet rs = psinsert.executeQuery();

                while (rs.next()) {
                    Object[] data = new Object[2];
                    data[0] = clientId;
                    data[1] = rs.getString("aco");
                    testDataList.add(data);
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return testDataList.iterator();
    }


    @DataProvider(name = ALL_CLIENTS)
    public static Iterator<Object[]> all_clients(ITestContext context) {
        List<Object[]> testData = new ArrayList<>();

        //{"client": 1001, "index": "0"}

        int[] cn;
        try {
            cn = Arrays.stream(context.getCurrentXmlTest().getParameter("client").split(","))
                    .mapToInt(Integer::parseInt).toArray();
        } catch (Exception ignored) {
            cn  = AppConfig.APP_SETTINGS.TEST_SETTINGS.clientList.stream().mapToInt(
                    e -> e).toArray();
        }


        for (Integer clientId : AppConfig.APP_SETTINGS.TEST_SETTINGS.clientList) {
            for (int i : cn) {
                if (i == clientId) {
                    setUsernames(Integer.toString(i), context);
                    Object[] a = new Object[1];
                    a[0] = Integer.toString(clientId);
                    testData.add(a);
                }
            }
        }
        return testData.iterator();
    }

    private static void setUsernames(String client, ITestContext context) {
        String username = AppConfig.APP_SETTINGS.USERNAME_SETTINGS.usernameMap.get(client);
        if (context.getCurrentXmlTest().getAllParameters().containsKey("username")) {
            String user = context.getCurrentXmlTest().getAllParameters().get("username");
            AppConfig.APP_SETTINGS.USERNAME_SETTINGS.usernameMap.replace(client, username, user);
        }
    }
}
