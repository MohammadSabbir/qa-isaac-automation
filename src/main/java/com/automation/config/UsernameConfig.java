package com.automation.config;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

public class UsernameConfig {

    @JsonProperty("username_map")
    public Map<String, String> usernameMap;
    public Map<String, Map<String, String>> config;

    @Override
    public String toString() {
        return "UsernameConfig{" +
                "usernameMap=" + usernameMap +
                ", config=" + config +
                '}';
    }
}
