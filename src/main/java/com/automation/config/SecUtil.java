package com.automation.config;


import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;

public class SecUtil {

    private static final String MASTER_KEY = "A5J4XkAU79qXkLol7GmqsoypyxlUosD4LTRh8MFx";
    private static final String ENCRYPTION_ALGORITHM = "PBEWITHSHA1ANDRC4_128";

    private static PooledPBEStringEncryptor encoder() {
        PooledPBEStringEncryptor enc = new PooledPBEStringEncryptor();
        enc.setPoolSize(4);
        enc.setPassword(MASTER_KEY);
        enc.setAlgorithm(ENCRYPTION_ALGORITHM);
        return enc;
    }

    public static String securedProperty(String value) {
        String decryptedProperty;
        if (null != value) {
            decryptedProperty = encoder().decrypt(value);
        } else {
            return null;
        }
        return decryptedProperty;
    }
}
