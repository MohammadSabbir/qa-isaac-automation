package com.automation.config;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class TestConfig {

    @JsonProperty("client-list")
    public List<Integer> clientList;

    @Override
    public String toString() {
        return "TestConfig{" +
                "clientList=" + clientList +
                '}';
    }
}
