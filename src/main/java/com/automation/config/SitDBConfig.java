package com.automation.config;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Map;

public class SitDBConfig {
    public String name;
    public String driver;
    public String host;
    public Map<String, String> dataSourceProperty;
    @JsonIgnore
    private static String table;
    public boolean connect;

    public String getTableName() {
        if (null == table) {
            switch (AppConfig.APP_SETTINGS.ENVIRONMENT_SETTINGS.environment) {
                case "sit-rc":
                    table = "cgpdashrc";
                    break;
                case "sit-main":
                    table = "cgpdash";
                    break;
                default:
                    throw new RuntimeException("cannot configure invalid db " +
                            AppConfig.APP_SETTINGS.SITDB_SETTINGS);
            }
        }
        return table;
    }

    @Override
    public String toString() {
        return "SitDBConfig{" +
                "name='" + name + '\'' +
                ", driver='" + driver + '\'' +
                ", host='" + host + '\'' +
                ", dataSourceProperty=" + dataSourceProperty +
                ", table='" + table + '\'' +
                '}';
    }
}
