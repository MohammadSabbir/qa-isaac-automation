package com.automation.config;

import java.util.List;
import java.util.Map;

/**
 * This class is a POJO for the environment context.
 */
public class EnvironmentConfig {

    public String environment;
    public List<Map<String, String>> testEnvironments;

    public String getUrl() {
        return testEnvironments.stream().filter(
                e -> environment.equals(e.get("name")))
                .findFirst().orElseThrow().get("url");
    }

    @Override
    public String toString() {
        return "TestEnvironment{" +
                "environment='" + environment + '\'' +
                ", testEnvironment=" + testEnvironments +
                '}';
    }
}
