package com.automation.config;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Objects;

/**
 * This class reads the json file and sets application context.
 */
public class AppConfig {


    /**
     * The directory of our settings.
     */
    private static String contextDir = "app-context";

    /**
     * Static App Settings, this is initialized at startup.
     */
    public static final AppConfig APP_SETTINGS;


    public EnvironmentConfig ENVIRONMENT_SETTINGS;
    public DriverConfig WEBDRIVER_SETTINGS;
    public SitDBConfig SITDB_SETTINGS;
    public QATeamDBConfig QATEAMDB_SETTINGS;
    public TestConfig TEST_SETTINGS;
    public UsernameConfig USERNAME_SETTINGS;
    /**
     * DB Queries file.
     */
    public File DB_QUERIES;

    static {
        APP_SETTINGS = new AppConfig(contextDir);
    }

    public AppConfig(String dir) {

        //Locate the directory for application context
        var files = getFileFromURL(dir);

        //Verify non-null
        if (null == files || null == files.listFiles()) {
            throw new NullPointerException(String.format("the directory: [%s] could not be found or the directory is empty", dir));
        }
        setConfigs(files.listFiles());
    }

    /**
     * this method reads the Json files and sets the static configs.
     * @param files
     */
    private void setConfigs(File[] files)  {
        ObjectMapper mapper = new ObjectMapper();
        for (File file : Objects.requireNonNull(files)) {
            try {
                switch (file.getName()) {
                    case "environment.json":
                        this.ENVIRONMENT_SETTINGS = mapper.readValue(file, EnvironmentConfig.class);
                        break;
                    case "browser.json":
                        this.WEBDRIVER_SETTINGS = mapper.readValue(file, DriverConfig.class);
                        break;
                    case "sitdb.json":
                        this.SITDB_SETTINGS = mapper.readValue(file, SitDBConfig.class);
                        break;
                    case "test.json":
                        this.TEST_SETTINGS = mapper.readValue(file, TestConfig.class);
                        break;
                    case "queries.json":
                       this.DB_QUERIES = file;
                        break;
                    case "qadb.json":
                        this.QATEAMDB_SETTINGS = mapper.readValue(file, QATeamDBConfig.class);
                        break;
                    case "Usernames.json":
                        this.USERNAME_SETTINGS = mapper.readValue(file, UsernameConfig.class);
                        break;
                    default:
                        break;
                }
            } catch (Exception e ) {
                System.out.println("FAILED TO LOAD JSON FILE" + file.getName());
                System.out.println(e.getLocalizedMessage());
            }
        }
    }

    /**
     * this method returns a file from a URL.
     * @param directory the directory or file to return
     * @return File (can be null).
     */
    private File getFileFromURL(String directory) {
        URL url = this.getClass().getClassLoader().getResource(directory);
        File file = null;
        try {
            file = new File(url.toURI());
        } catch (URISyntaxException e) {
            file = new File(url.getPath());
        } finally {
            return file;
        }
    }

    @Override
    public String toString() {
        return "AppConfig{" +
                "ENVIRONMENT_SETTINGS=" + ENVIRONMENT_SETTINGS +
                ", WEBDRIVER_SETTINGS=" + WEBDRIVER_SETTINGS +
                ", SITDB_SETTINGS=" + SITDB_SETTINGS +
                ", QATEAMDB_SETTINGS=" + QATEAMDB_SETTINGS +
                ", TEST_SETTINGS=" + TEST_SETTINGS +
                ", USERNAME_SETTINGS=" + USERNAME_SETTINGS +
                ", DB_QUERIES=" + DB_QUERIES +
                '}';
    }
}
