package com.automation.config;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Map;

@JsonIgnoreProperties({"qa"})
public class DBQueries {
    public Map<String, Map<String, Map<String, String>>> sit;
    public Map<String, Map<String, Map<String, String>>> qa;
}
