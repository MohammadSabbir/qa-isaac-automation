package com.automation.config;

import java.util.Map;

public class QATeamDBConfig {
    public String name;
    public String driver;
    public String host;
    public Map<String, String> dataSourceProperty;
    public boolean connect;

    @Override
    public String toString() {
        return "QATeamDBConfig{" +
                "name='" + name + '\'' +
                ", driver='" + driver + '\'' +
                ", host='" + host + '\'' +
                ", dataSourceProperty=" + dataSourceProperty +
                ", connect=" + connect +
                '}';
    }
}
