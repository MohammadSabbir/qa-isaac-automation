package com.automation.config;

import java.util.List;

/**
 * This class is a POJO for Driver-Context.
 */
public class DriverConfig {
    public String browser;
    public boolean headless;
    public boolean local;
    public List<String> chromeOptions;
    public boolean hideDriverLogs;
    public String downloadPath;

    @Override
    public String toString() {
        return "DriverConfig{" +
                "browser='" + browser + '\'' +
                ", headless=" + headless +
                ", local=" + local +
                ", chromeOptions=" + chromeOptions +
                '}';
    }
}
