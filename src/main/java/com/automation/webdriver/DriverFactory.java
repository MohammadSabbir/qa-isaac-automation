package com.automation.webdriver;

import com.automation.config.AppConfig;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;

import java.util.Collections;
import java.util.HashMap;

/**
 * This class is a utility for creating an instance of webdriver.
 */
public class DriverFactory {

    private final String browser;

    private DriverFactory() {
        this.browser = AppConfig.APP_SETTINGS.WEBDRIVER_SETTINGS.browser;
    }

    public static WebDriver getDriver() {
        DriverFactory driverFactory = new DriverFactory();

        switch (driverFactory.browser) {
            case "chrome":
                driverFactory.getChromeOptions();
                return driverFactory.getChromeDriver();
            case "firefox":
                return driverFactory.getFirefoxDrier();
            default:
                throw new RuntimeException("this browser is not supported yet");
        }
    }

    private WebDriver getFirefoxDrier() {
        WebDriverManager.firefoxdriver().setup();
        return new FirefoxDriver();
    }

    private WebDriver getChromeDriver() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = getChromeOptions();
        return new ChromeDriver(options);
    }

    private ChromeOptions getChromeOptions() {
        ChromeOptions options = new ChromeOptions();
        for (String chromeOption : AppConfig.APP_SETTINGS.WEBDRIVER_SETTINGS.chromeOptions) {
            options.addArguments(chromeOption);
        }
        hideDriverLogs(options);
        setDownloadPath(options);
        return options;
    }

    private void hideDriverLogs(ChromeOptions options) {
        if (AppConfig.APP_SETTINGS.WEBDRIVER_SETTINGS.hideDriverLogs) {
            options.addArguments("log-level=2");
            System.setProperty("webdriver.chrome.silentOutput", "true");
            java.util.logging.Logger.getLogger("org.openqa.selenium").setLevel(java.util.logging.Level.SEVERE);
        }
    }

    private void setDownloadPath(ChromeOptions options) {

        // Set the Path of the downloads
        HashMap<String, Object> chromePrefs = new HashMap<>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        chromePrefs.put("download.default_directory", AppConfig.APP_SETTINGS.WEBDRIVER_SETTINGS.downloadPath);

        options.setExperimentalOption("prefs", chromePrefs);
        options.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
        options.setCapability("chrome.switches", Collections.singletonList("--incognito"));
        options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        options.setCapability(ChromeOptions.CAPABILITY, options);
    }
}
