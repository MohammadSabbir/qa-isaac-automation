package com.automation.database;

import com.automation.config.AppConfig;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

public class DataSources {

    private static final Connection qa;
    private static final Connection sit;

    static  {
        Connection sit1 = null;
        HikariConfig sitConfig;
        if (AppConfig.APP_SETTINGS.SITDB_SETTINGS.connect) {
            sitConfig = setConfig(
                    AppConfig.APP_SETTINGS.SITDB_SETTINGS.host,
                    AppConfig.APP_SETTINGS.SITDB_SETTINGS.driver,
                    AppConfig.APP_SETTINGS.SITDB_SETTINGS.getTableName(),
                    AppConfig.APP_SETTINGS.SITDB_SETTINGS.dataSourceProperty);

            var sitDataSource = new HikariDataSource(sitConfig);
            Runtime.getRuntime().addShutdownHook(new Thread(sitDataSource::close));

            try {
                sit1 = sitDataSource.getConnection();
            } catch (SQLException throwables) {
                throwables.printStackTrace();;
            }
        }

        sit = sit1;

        Connection qa1 = null;
        HikariConfig qaConfig;
        if (AppConfig.APP_SETTINGS.QATEAMDB_SETTINGS.connect) {
            qaConfig = setConfig(
                    AppConfig.APP_SETTINGS.QATEAMDB_SETTINGS.host,
                    AppConfig.APP_SETTINGS.QATEAMDB_SETTINGS.driver,
                    AppConfig.APP_SETTINGS.QATEAMDB_SETTINGS.name,
                    AppConfig.APP_SETTINGS.QATEAMDB_SETTINGS.dataSourceProperty);

            var qaDataSource = new HikariDataSource(qaConfig);
            Runtime.getRuntime().addShutdownHook(new Thread(qaDataSource::close));

            try {
                qa1 = qaDataSource.getConnection();
            } catch (SQLException throwable) {
                throwable.printStackTrace();
            }
        }
        qa = qa1;
    }

    private static HikariConfig setConfig(String host, String driver, String tableName, Map<String,String> dsProps) {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(driver);

        hikariConfig.setJdbcUrl(String.format("jdbc:postgresql://%s/%s", host, tableName));

        String username = "qateam_user";
        String password = "MByg7FfQqaX6AJFu";

        hikariConfig.setUsername(username);
        hikariConfig.setPassword(password);

        hikariConfig.setAutoCommit(true);
        hikariConfig.setMaximumPoolSize(100);
        hikariConfig.setMinimumIdle(1);
        hikariConfig.setConnectionTestQuery("SELECT 1");
        hikariConfig.setConnectionTimeout(40_000);
        hikariConfig.setIdleTimeout(60_000);
        hikariConfig.setValidationTimeout(5_000);

        dsProps.forEach(hikariConfig::addDataSourceProperty);
        return hikariConfig;
    }

    public static Connection getQADatabase() {
        return qa;
    }

    public static Connection getSitDatabase() {
        return sit;
    }

    public static void main(String[] args) {

    }
}
