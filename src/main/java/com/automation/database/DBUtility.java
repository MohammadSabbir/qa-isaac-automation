package com.automation.database;

import com.automation.config.AppConfig;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.*;

/**
 * This class is a utility that sends and retrieves queries to the test database.
 */
public class DBUtility {

    private static DBUtility instance;
    public final JsonNode root;
    private final Connection sit;
    private final Connection qa;

    private DBUtility() {
        JsonNode root1;
        if (null == instance) {
            synchronized (this) {
                instance = this;
            }
        }

        this.sit = DataSources.getSitDatabase();
        this.qa = DataSources.getQADatabase();

        ObjectMapper mapper = new ObjectMapper();
        try {
            root1 = mapper.readTree(AppConfig.APP_SETTINGS.DB_QUERIES);
        } catch (IOException ioException) {
            System.out.println("failed to open queries file " + ioException.getLocalizedMessage());
            root1 = null;
        }
        this.root = root1;
    }

    private static Connection getConnection(String query) {
        if (query.startsWith("sit.")) {
            return getInstance().sit;
        } else if (query.startsWith("qa.")) {
            return getInstance().qa;
        } else {
            throw new RuntimeException("invalid query :" + query);
        }
    }

    private static DBUtility getInstance() {
        if (null == instance) {
            new DBUtility();
        }
        return instance;
    }

    private static String getQuery(String query) {
        JsonNode jsonNode = getInstance().root;
        for (String s : query.split("\\.")) {
            jsonNode = jsonNode.get(s);
        }
        return Objects.requireNonNull(jsonNode.asText());
    }

    public static List<Map<String, String>> fetch(String queryId, String[] columns, String... args) {

        try (Connection c = getConnection(queryId)) {
            String q = getQuery(queryId);
            String format = MessageFormat.format(q, args);
            PreparedStatement psinsert = c.prepareStatement(format);
            ResultSet rs = psinsert.executeQuery();

            List<Map<String, String>> rows = new ArrayList<>();

            while (rs.next()) {
                Map<String, String> map = new HashMap<>();
                for (String column : columns) {
                    map.put(column, rs.getString(column));
                }
                rows.add(map);
            }
            return rows;

        } catch (SQLException throwables) {
            System.out.println("Failed to fetch data ");
            throwables.printStackTrace();
        }
        throw new RuntimeException();
    }

    public static void push(String query) {
    }

    public static void main(String[] args) throws SQLException {
        String queryId = "sit.data-provider.all";
        String[] columns = {"aco"};
        String[] arg = {"1001"};
        System.out.println(fetch(queryId, columns, arg));
    }
}
