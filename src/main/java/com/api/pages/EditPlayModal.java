package com.api.pages;

import com.api.modals.BaseModal;
import com.api.pagefactory.EditPlaySummaryPageFactory;
import com.api.util.Click;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EditPlayModal extends BaseModal {

    private final EditPlaySummaryPageFactory page;

    public EditPlayModal(WebDriver driver) {
        super(driver);
        this.page = PageFactory.initElements(getDriver(), EditPlaySummaryPageFactory.class);
    }

    @Override
    public boolean isOpen() {
        return false;
    }

    public EditPlaySummaryPageFactory getPage() {
        return page;
    }

    public void deletePlay() {
        new WebDriverWait(getDriver(), 15).until(
                ExpectedConditions.elementToBeClickable(
                        getPage().getDeleteButton())
        );
        Click.element(getDriver(), getPage().getDeleteButton());

        new WebDriverWait(getDriver(), 15).until(
                ExpectedConditions.numberOfElementsToBeMoreThan(
                        By.xpath("//*[contains(@class,'modal') and contains(@class,'delete')]//*[@value='OK']"),
                        0)
        );

        Click.element(getDriver(), getPage().getDeleteConfirmationButton().get(0));

        new WebDriverWait(getDriver(), 15).until(
                ExpectedConditions.attributeContains(
                        getPage().getAdvisorHeaderLabel(),
                        "innerText",
                        "Playbook"
                )
        );
    }
}
