package com.api.pages;

import com.automation.config.AppConfig;
import com.automation.config.SecUtil;
import com.api.pagefactory.HomepagePageFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class Homepage extends BasePage {

    private final HomepagePageFactory page;

    public Homepage(WebDriver driver) {
        super(driver);
        this.page = PageFactory.initElements(driver, HomepagePageFactory.class);
    }

    public void login(String username) {
        String s = null;
        try {
            s = AppConfig.APP_SETTINGS.USERNAME_SETTINGS.config.get(username).get("password");
        } catch (Exception e) {
            e.printStackTrace();
        }


        while (page.getUsernameList().size() > 0) {
            page.getUsernameInput().clear();
            page.getPasswordInput().clear();

            while (!page.getUsernameInput().getAttribute("value").equals(username)) {
                page.getUsernameInput().sendKeys(username);
            }
            while (!page.getPasswordInput().getAttribute("value").equals(SecUtil.securedProperty(s))) {
                page.getPasswordInput().sendKeys(SecUtil.securedProperty(s));
            }
            page.getSubmit().click();
            load();
        }
    }
}
