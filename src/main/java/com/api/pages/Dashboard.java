package com.api.pages;

import com.api.aco.AcoGraph;
import com.api.aco.DashboardAco;
import com.api.pagefactory.DashboardPageFactory;
import com.api.util.Click;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Dashboard extends BasePage {

    private static Map<String, AcoGraph<DashboardAco>> acoGraphs;
    private DashboardPageFactory page;

    public Dashboard(WebDriver driver) {
        super(driver);
    }

    public DashboardPageFactory getPage() {
        if (null == page) {
            this.page = PageFactory.initElements(getDriver(), DashboardPageFactory.class);
        }
        return page;
    }

    public String getTotalOpportunity() {
        try {
            return getPage().getAllTotalOpportunity().getAttribute("innerText");
        } catch (Exception e) {
            return null;
        }
    }

    public Double getTotalOppFromChiclet() {

        String da = null;
        try {
            da = getPage().getAllTotalOpportunity().getAttribute("data-amount");
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (null != da) {
            return Double.parseDouble(da);
        }
        else {
            return 0.0;
        }
    }

    public boolean isChicletClickable(String ckltId) {
        WebElement chiclet = getPage().getChicletsAndOpportunities().stream()
                .filter(e -> e.getAttribute("id").equals(ckltId))
                    .findFirst().orElse(null);

        return null != chiclet && chiclet.getAttribute("data-href").contains("1");
    }

    private synchronized AcoGraph<DashboardAco> getGraph(String username) {
        if (acoGraphs.containsKey(username)) {
            return acoGraphs.get(username);
        }
        return null;
    }

    private synchronized void addToGraph(String username, AcoGraph<DashboardAco> dashboardAcoAcoGraph) {
        acoGraphs.put(username, dashboardAcoAcoGraph);
    }

    public DashboardAco getAco(String aco) {
        initGraph();
        //No All level on the dashboard
        if (aco.equalsIgnoreCase("all")) {
            DashboardAco a = new DashboardAco(aco, 0);
            for (DashboardAco dashboardAco : Objects.requireNonNull(getGraph(getUsername())).getLevel0()) {
                a.addChild(dashboardAco);
            }
            //Create ACO Object
            return a;
        }
        return Objects.requireNonNull(getGraph(getUsername())).getAco(aco);
    }

    public List<DashboardAco> getAllAcos() {
        initGraph();
        return Objects.requireNonNull(getGraph(getUsername())).getAcoList();
    }

    public List<DashboardAco> getLvl1s() {
        initGraph();
        return Objects.requireNonNull(getGraph(getUsername())).getLevel1();
    }

    public List<DashboardAco> getLvl0s() {
        initGraph();
        return Objects.requireNonNull(getGraph(getUsername())).getLevel0s();
    }

    public List<DashboardAco> getLvl2s() {
        initGraph();
        return Objects.requireNonNull(getGraph(getUsername())).getLevel2();
    }


    /**
     * Close all the parent arrows on the dashboard hierarchy.
     */
    private void closeParentArrows() {
        if (getPage().getContractArrows().size() >= 1) {
            for (WebElement parentArrow : getPage().getContractArrows()) {
                String openIcon = parentArrow.getAttribute("class");

                if (openIcon.contains("open")) {
                    Click.element(getDriver(), parentArrow);
                }
            }
        }
    }

    private AcoGraph<DashboardAco> buildAcoGraph() {
        Function<Integer, String> getDC = index ->
                getPage().getContracts().get(index).getAttribute("data-contract");

        List<DashboardAco> collect = IntStream.range(0, getPage().getContracts().size())
                .mapToObj(e -> new DashboardAco(getDC.apply(e), e))
                .collect(Collectors.toList());

        return new AcoGraph<>(DashboardAco.class, collect);
    }

    private void initGraph() {
        if (null == acoGraphs) {
            synchronized (this) {
                if (null == acoGraphs) {
                    acoGraphs = new HashMap<>();
                }
            }
        }

        if (null == getGraph(getUsername())) {
            buildAcoGraph();
            addToGraph(getUsername(), buildAcoGraph());
        }
    }
}


