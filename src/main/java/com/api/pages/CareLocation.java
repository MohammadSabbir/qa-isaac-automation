package com.api.pages;

import com.api.pagefactory.CareLocationPageFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class CareLocation extends SavingsAdvisor {

    private final CareLocationPageFactory page;

    public CareLocation(WebDriver driver) {
        super(driver);
        this.page = PageFactory.initElements(getDriver(), CareLocationPageFactory.class);
    }

    public CareLocationPageFactory getPage() {
        return page;
    }

    public int getNumberOfPathways() {
        return getPage().getPathways().size();
    }

    public int getNumberOfAttributedPhysicians() {
        return getPage().getAttributedPhysicians().size();
    }

    public void selectAttributedPhysician(int index) {
        selectBarchart(getPage().getAttributedPhysicians().get(index));
        load();
    }

    public void selectPathway(int index) {
        selectBarchart(getPage().getPathways().get(index));
        load();
    }
}
