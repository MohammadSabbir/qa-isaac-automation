package com.api.pages;

import com.api.pagefactory.EditPlayPageFactory;
import com.api.util.Click;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.function.Predicate;

public class EditPlaySummary extends BasePage {

    private final EditPlayPageFactory page;

    public EditPlaySummary(WebDriver driver) {
        super(driver);
        this.page = PageFactory.initElements(getDriver(), EditPlayPageFactory.class);
    }

    public EditPlayPageFactory getPage() {
        return page;
    }

    public EditPlayModal openPlay(String playName) {
        new WebDriverWait(getDriver(), 360).until(
                ExpectedConditions.numberOfElementsToBeMoreThan(
                        By.xpath("//tr[contains(@id,'play-id')]//td[contains(@class,'playName')]"),
                        0
                )
        );

        WebElement title = findFromPlayName(e -> e.getAttribute("title").equals(playName));
        Click.move(getDriver(), title);
        Click.element(getDriver(), title);

        new WebDriverWait(getDriver(), 20).until(
                ExpectedConditions.attributeContains(
                        getPage().getAdvisorHeaderLabel(),
                        "innerText",
                        "Playbook Detail"
                )
        );
        return new EditPlayModal(getDriver());
    }

    private WebElement findFromPlayName(Predicate<WebElement> test) {
        return getPage().getPlayNames().stream().filter(test).findFirst().orElseThrow();
    }
}
