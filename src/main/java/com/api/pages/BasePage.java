package com.api.pages;

import com.api.pagefactory.NavigationMenuPageFactory;
import com.api.util.Click;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public abstract class BasePage extends Pages {

    public BasePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(new AjaxElementLocatorFactory(getDriver(),30), this);
    }

    public String getHeader() {
        NavigationMenuPageFactory commonPage = PageFactory.initElements(getDriver(), NavigationMenuPageFactory.class);
        return commonPage.getAdvisorHeaderLabel().getAttribute("innerText");
    }

    public Boolean clientLogoDisplayed() {
        NavigationMenuPageFactory commonPage = PageFactory.initElements(getDriver(), NavigationMenuPageFactory.class);
        boolean naturalWidth = commonPage.getClientLogos().stream().noneMatch(e -> e.getAttribute("naturalWidth").equals("0"));
        int numberOfImages = commonPage.getClientLogos().size();
        return naturalWidth && numberOfImages == 2;
    }

    public String getUsername() {
        NavigationMenuPageFactory commonPage = PageFactory.initElements(getDriver(), NavigationMenuPageFactory.class);
        return commonPage.getUserInfo().getAttribute("innerText");
    }

    protected void selectBarchart(WebElement element) {
        Click.elementUntilTrue(getDriver(),
                element, e -> e.getAttribute("class").contains("selected"));
    }
}
