package com.api.pages;

import com.api.pagefactory.ClinicalPageFactory;
import com.api.util.Loader;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class ClinicalPage extends SavingsAdvisor {

    private ClinicalPageFactory page;

    public ClinicalPage(WebDriver driver) {
        super(driver);
    }

    public ClinicalPageFactory getPage() {
        if (null == page) {
            this.page = PageFactory.initElements(getDriver(), ClinicalPageFactory.class);
        }
        return page;
    }

    public int getNumberOfVariations() {
        return getPage().getClinicalVariations().size();
    }

    public int getNumberOfPathways() {
        return getPage().getPathways().size();
    }

    public int getNumberOfAttributedPhysicians() {
        return getPage().getAttributedPhysicians().size();
    }

    public void selectVariation(int index) {
        selectBarchart(getPage().getClinicalVariations().get(index));
        load();
    }
}
