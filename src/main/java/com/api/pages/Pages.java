package com.api.pages;

import com.api.util.Loader;
import org.openqa.selenium.WebDriver;

public class Pages {

    private final WebDriver driver;

    public Pages(WebDriver d) {
        this.driver = d;
    }

    public WebDriver getDriver() {
        return driver;
    }

    /**
     * Waits for the ISAAC Spinner to disappear. Max time by default is 360
     * Seconds.
     */
    public void load() {
        Loader.load(getDriver());
    }
}
