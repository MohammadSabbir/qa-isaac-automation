package com.api.pages;

import com.api.modals.ActionBarModal;
import com.api.modals.PPDModal;
import com.api.modals.PatientVolumeModal;
import com.api.pagefactory.SavingsAdvisorPageFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

abstract class SavingsAdvisor extends BasePage {

    private SavingsAdvisorPageFactory page;
    private ActionBarModal actionBarModal;
    private PatientVolumeModal patientVolume;
    private PPDModal ppdModal;

    public SavingsAdvisor(WebDriver driver) {
        super(driver);
    }

    public PPDModal getPpdModal() {
        if (null == ppdModal) {
            this.ppdModal = new PPDModal(getDriver());
        }
        return ppdModal;
    }

    public ActionBarModal getActionBarModal() {
        if (null == actionBarModal) {
            this.actionBarModal = new ActionBarModal(getDriver());
        }
        return actionBarModal;
    }

    public SavingsAdvisorPageFactory getPage() {
        if (null == page) {
            this.page = PageFactory.initElements(getDriver(), SavingsAdvisorPageFactory.class);
        }
        return page;
    }

    public void selectPathway(int index) {
        selectBarchart(getPage().getPathways().get(index));
        load();
    }

    public void selectAttributedPhysician(int index) {
        selectBarchart(getPage().getAttributedPhysicians().get(index));
        load();
    }

    public PatientVolumeModal getPatientVolume() {
        if (null == patientVolume) {
            this.patientVolume = new PatientVolumeModal(getDriver());
        }
        return patientVolume;
    }


}
