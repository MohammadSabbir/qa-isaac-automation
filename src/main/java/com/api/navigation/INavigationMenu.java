package com.api.navigation;

import com.api.pagefactory.NavigationMenuPageFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public interface INavigationMenu {
    void navigate(WebDriver d);
    default NavigationMenuPageFactory getPage(WebDriver d) {
        return PageFactory.initElements(d, NavigationMenuPageFactory.class);
    }
}
