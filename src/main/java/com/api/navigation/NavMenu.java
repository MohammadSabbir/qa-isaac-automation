package com.api.navigation;

import com.api.pages.Pages;
import com.api.util.Click;
import com.api.util.IRetryFunction;
import com.api.util.RetryUtilImpl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

@SuppressWarnings("unchecked")
public enum NavMenu implements INavigationMenu {

    DASHBOARD {
        @Override
        public void navigate(WebDriver d) {
            NavMenu.nav(this, d, (WebElement element) ->
                            element.getAttribute("id").contains("executiveDashboard"));
        }
    }, NETWORK {
        @Override
        public void navigate(WebDriver d) {
            NavMenu.nav(this, d, (WebElement element) ->
                            element.getAttribute("innerText").contains("ADVISORS"),
                    element -> element.getAttribute("id").contains("networkReferralOptimization"));
        }
    }, CARE_LOCATION {
        @Override
        public void navigate(WebDriver d) {
            NavMenu.nav(this, d, (WebElement element) ->
                            element.getAttribute("innerText").contains("ADVISORS"),
                    element -> element.getAttribute("id").contains("inpatientAdvisor"));
        }
    }, PHYSICIAN {
        @Override
        public void navigate(WebDriver d) {
            NavMenu.nav(this, d, (WebElement element) ->
                            element.getAttribute("innerText").contains("ADVISORS"),
                    element -> element.getAttribute("id").contains("physicianAdvisor"));
        }
    }, L_FACILITY {
        @Override
        public void navigate(WebDriver d) {
            NavMenu.nav(this, d, (WebElement element) ->
                            element.getAttribute("innerText").contains("ADVISORS"),
                    element -> element.getAttribute("innerText").contains("Leakage"),
                    element -> element.getAttribute("id").equals("leakageFacility"));
        }
    }, L_PHYSICIAN {
        @Override
        public void navigate(WebDriver d) {
            NavMenu.nav(this, d, (WebElement element) ->
                            element.getAttribute("innerText").contains("ADVISORS"),
                    element -> element.getAttribute("innerText").contains("Leakage"),
                    element -> element.getAttribute("id").equals("leakageProfessional"));
        }
    }, MP_BUNDLES {
        @Override
        public void navigate(WebDriver d) {
            NavMenu.nav(this, d, (WebElement element) ->
                            element.getAttribute("innerText").toLowerCase().contains("make a play"),
                    element -> element.getAttribute("id").contains("bundlesAdvisor"));
        }
    },
    MP_PERFORMANCE {
        @Override
        public void navigate(WebDriver d) {
            NavMenu.nav(this, d, (WebElement element) ->
                            element.getAttribute("innerText").toLowerCase().contains("make a play"),
                    element -> element.getAttribute("id").contains("prescriptiveAdvisor"));
        }
    }, PB_SAVINGS {
        @Override
        public void navigate(WebDriver d) {
            NavMenu.nav(this, d, (WebElement element) ->
                            element.getAttribute("innerText").toLowerCase().contains("playbook"),
                    element -> element.getAttribute("id").equals("playbookSavings"));
        }
    }, PB_LEAKAGE {
        @Override
        public void navigate(WebDriver d) {
            NavMenu.nav(this, d, (WebElement element) ->
                            element.getAttribute("innerText").toLowerCase().contains("playbook"),
                    element -> element.getAttribute("id").contains("playbookSavings_Revenue"));
        }
    }, PB_PERFORMANCE {
        @Override
        public void navigate(WebDriver d) {
            NavMenu.nav(this, d, (WebElement element) ->
                            element.getAttribute("innerText").toLowerCase().contains("playbook"),
                    element -> element.getAttribute("id").contains("playbookPerformance"));
        }
    }, PB_BUNDLES {
        @Override
        public void navigate(WebDriver d) {
            NavMenu.nav(this, d, (WebElement element) -> element.getAttribute("innerText").contains("Playbook")
                    , element -> element.getAttribute("id").equalsIgnoreCase("BundlesPlaybookSummary"));
        }
    }, CLINICAL {
        @Override
        public void navigate(WebDriver d) {
            NavMenu.nav(this, d, (WebElement element) ->
                            element.getAttribute("innerText").contains("ADVISORS"),
                    element -> element.getAttribute("innerText").contains("Clinical"),
                    element -> element.getAttribute("id").equalsIgnoreCase("clinicalAdvisor"));
        }
    };

    private static void nav(NavMenu nav, WebDriver d, Predicate<WebElement>... menuTests) {

        List<Integer> indices = new ArrayList<>();
        List<WebElement> allOptionsOnNavDropdown = nav.getPage(d).getAllOptionsOnNavDropdown();
        for (int i = 0, allOptionsOnNavDropdownSize = allOptionsOnNavDropdown.size(); i < allOptionsOnNavDropdownSize; i++) {
            WebElement element = allOptionsOnNavDropdown.get(i);
            for (Predicate<WebElement> menuTest : menuTests) {
                if (menuTest.test(element)) {
                    indices.add(i);
                    break;
                }
            }
        }

        RetryUtilImpl.retry(5, new IRetryFunction() {
            @Override
            public void doThis() {
                for (Integer index : indices) {
                    var element = nav.getPage(d).getAllOptionsOnNavDropdown().get(index);
                    Click.move(d, element);
                    Click.element(d, element);

                    if ((index + 1) < indices.size()) {
                        new WebDriverWait(d, 2).until(ExpectedConditions.visibilityOf(
                                nav.getPage(d).getAllOptionsOnNavDropdown().get(index + 1)));
                    }
                }
                var page = PageFactory.initElements(d, Pages.class);
                page.load();
            }

            @Override
            public void logErrors(Exception e) {
                e.printStackTrace();
            }
        });
    }
}
