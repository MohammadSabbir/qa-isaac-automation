package com.api.modals;

import com.api.pagefactory.PPDModalPageFactory;
import com.api.util.Click;
import com.api.util.IRetryFunction;
import com.api.util.Loader;
import com.api.util.RetryUtilImpl;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.stream.Collectors;

public class PPDModal extends BaseModal {

    private PPDModalPageFactory page;

    public PPDModal(WebDriver driver) {
        super(driver);
        Loader.load(getDriver());
        new WebDriverWait(getDriver(), 10).until(ExpectedConditions
                .elementToBeClickable(getPage().getCategoryDropdown()));
    }

    private String getCurrentCategory() {
        return getPage().getCategoryDropdownLabel()
                .getAttribute("innerText").replaceFirst("\n", "");
    }

    private int getNumberOfOpportunities() {
        return getPage().getOpportunityBarcharts().size();
    }

    private int getNumberOfPathways() {
        return getPage().getPathways().size();
    }

    public String getTitle() {
        if (isOpen()) {
            return getPage().getModalTitle().getAttribute("innerText")
                    .replaceAll("\\u00a0", "");
        }
        return null;
    }

    /**
     * @return true if the Member Detail Button is enabled for this client.
     */
    public boolean isMemberDetailButtonDisplayed() {
        return getPage().getMemberDetailButton().size() > 0;
    }
    public PPDModalPageFactory getPage() {
        if (null == page) {
            this.page = PageFactory.initElements(getDriver(), PPDModalPageFactory.class);
        }
        return page;
    }

    public int getPathwaySize() {
        return getPage().getPathways().size();
    }

    public int getOpportunitySize() {
        return getPage().getOpportunityBarcharts().size();
    }

    public String getCategory() {
        return getPage().getCategoryDropdownLabel().getAttribute("innerText");
    }

    @Override
    public boolean isOpen() {
        return getPage().getModalWindow().size() > 0;
    }

    public void openCategoryDropdown() {
        new WebDriverWait(getDriver(), 10).until(ExpectedConditions
        .elementToBeClickable(getPage().getCategoryDropdown()));

        RetryUtilImpl.retry(10, new IRetryFunction() {
            @Override
            public void doThis() {
                Click.element(getDriver(), getPage().getCategoryDropdown());
                new WebDriverWait(getDriver(), 5).until(
                        ExpectedConditions.visibilityOfAllElements(
                                getPage().getCategoryDropdownOptions()
                        ));
            }

            @Override
            public void logErrors(Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void selectOpportunityDetail(int index) {
        selectBarchart(getPage().getOpportunityBarcharts().get(index));
    }

    public void selectPathway(int index) {
        selectBarchart(getPage().getPathways().get(index));
    }

    public void  selectCategoryDropdown(String categoryName) {
        openCategoryDropdown();
        System.out.println(getPage().getCategoryDropdownOptions().stream().map(WebElement::getText).collect(Collectors.toList()));
        getPage().getCategoryDropdownOptions().stream()
                .filter(e -> e.getAttribute("innerText").equalsIgnoreCase(categoryName))
                .findFirst().ifPresentOrElse(e -> Click.element(getDriver(), e), () -> {
            throw new RuntimeException(String.format("Cant click %s", categoryName));
                });
    }

    @Override
    public void close() {
        RetryUtilImpl.retry(10, new IRetryFunction() {
            @Override
            public void doThis() {
               Click.element(getDriver(), page.getCloseModal());
                new WebDriverWait(getDriver(), 5).until(d -> !isOpen());
            }

            @Override
            public void logErrors(Exception e) {
                e.printStackTrace();
            }
        });
    }
}
