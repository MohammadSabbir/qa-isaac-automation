package com.api.modals;

import com.api.pagefactory.PatientVolumeModalPageFactory;
import com.api.util.Click;
import com.api.util.IRetryFunction;
import com.api.util.Loader;
import com.api.util.RetryUtilImpl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PatientVolumeModal extends BaseModal {
    private final PatientVolumeModalPageFactory page;
    private final boolean enabled;
    private boolean open;
    private String minValue;
    private int min = 0;
    private String maxValue;
    private int max;
    private String savingsValue;
    private double savings;

    public PatientVolumeModal(WebDriver d) {
        super(d);
        this.page = PageFactory.initElements(d, PatientVolumeModalPageFactory.class);
        this.enabled = !page.getDropdown().getAttribute("class").contains("disabled");
        this.open = page.getDropdownOpen().size() > 0;
        if (enabled && open) {

            //Set Minimums
            this.minValue = page.getMarks().get(0).getAttribute("innerText");
            if (!minValue.isEmpty()) {
                min = Integer.parseInt(minValue);
            }

            //Set Maximums
            this.maxValue = (page.getMarks().get(page.getMarks().size() - 1)).getAttribute("innerText");
            if (!maxValue.isEmpty()) {
                max = Integer.parseInt(maxValue);
            } else {
                max = page.getMarks().size() - 1;
            }

            //Set the Savings Level
            this.savingsValue = page.getSavings().getAttribute("innerText").replace("\n", "");
            this.savings = Double.parseDouble(page.getSavings().getAttribute("data-amount"));
        }
    }

    @Override
    public boolean isOpen() {
        return open;
    }

    protected PatientVolumeModalPageFactory getPage() {
        return page;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public String getMinValue() {
        return minValue;
    }

    public int getMin() {
        return min;
    }

    public String getMaxValue() {
        return maxValue;
    }

    public int getMax() {
        return max;
    }

    public String getSavingsValue() {
        return savingsValue;
    }

    public double getSavings() {
        return savings;
    }

    public PatientVolumeModal open() {
        final Boolean[] boo = new Boolean[1];
        RetryUtilImpl.retry(5, new IRetryFunction() {
            @Override
            public void doThis() {
                if (page.getDropdownOpen().size() == 0) {
                    Click.element(page.getDriver(), page.getDropdown());
                }
                boo[0] = new WebDriverWait(getPage().getDriver(), 5).until(d1 ->
                        page.getDropdownOpen().size() > 0);
                Loader.load(getPage().getDriver());
            }

            @Override
            public void logErrors(Exception e) {
                System.out.println("Failed to open patient volume dropdown");
                System.out.println(e.getLocalizedMessage());
            }
        });

        this.open = boo[0];
        return new PatientVolumeModal(getPage().getDriver());
    }

    public int getBucketValueAtIndex(int n) {
        String innerText = this.page.getMarks().get(n).getAttribute("innerText");
        if (!innerText.isEmpty() && !innerText.isBlank()) {
            return Integer.parseInt(innerText);
        }
        return 0;
    }

    public void setRange(int min, int max) {
        var minPlace = recursiveFilter(min, 0, page.getMarks().size());
        var maxPlace = recursiveFilter(max, 0, page.getMarks().size());

        Actions act = new Actions(getDriver());
        act.dragAndDrop(page.getMin(), page.getPhysicialMark().get(minPlace)).build().perform();
        act.dragAndDrop(page.getMax(), page.getPhysicialMark().get(maxPlace)).build().perform();
    }

    public void save() {
        RetryUtilImpl.retry(5, new IRetryFunction() {
            @Override
            public void doThis() {
                new WebDriverWait(getDriver(), 10).until(
                        ExpectedConditions.elementToBeClickable(page.getSave()));
                Click.element(getDriver(), page.getSave());
                Loader.load(getDriver());

                new WebDriverWait(getDriver(), 5).until(d1 ->
                        page.getMarks().size() == 0);
                closeNoDataFoundAlert();
            }

            @Override
            public void logErrors(Exception e) {
                System.out.println("Failed to Save PVF after filtering ");
                System.out.println(e.getLocalizedMessage());
            }
        });
    }

    @Override
    public void close() {

    }

    private int recursiveFilter(int range, int startingPoint, int endingPoint) {
        //Index at the middle pointx
        int mid = startingPoint + (endingPoint - startingPoint) /   2;
        int valueAtMid = getBucketValueAtIndex(mid); //7

        if (valueAtMid > range) {
            mid--;
            //Check a level below, if a level below is < Range, we should return mid;
            if (getBucketValueAtIndex(mid) < range) {
                return mid;
            } else {
                return recursiveFilter(range, startingPoint, mid);
            }

        } else if (valueAtMid < range) {
            mid++;
            if (getBucketValueAtIndex(mid) > range) {
                return mid;
            } else {
                //Check a level up, if a level up is > Range, we should return mid;
                return recursiveFilter(range, valueAtMid, endingPoint);
            }
        }
        return mid;
    }

    @Override
    public String toString() {
        return "PatientVolumeModal{" +
                "enabled=" + enabled +
                ", open=" + open +
                ", minValue='" + minValue + '\'' +
                ", min=" + min +
                ", maxValue='" + maxValue + '\'' +
                ", max=" + max +
                ", savingsValue='" + savingsValue + '\'' +
                ", savings=" + savings +
                '}';
    }
}
