package com.api.modals;

import com.api.pagefactory.BasePageFactory;
import com.api.pages.BasePage;
import com.api.util.Click;
import com.api.util.IRetryFunction;
import com.api.util.Loader;
import com.api.util.RetryUtilImpl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class BaseModal extends BasePage {

    private BasePageFactory base;

    public BaseModal(WebDriver driver) {
        super(driver);
    }

    public abstract boolean isOpen();

    public BasePageFactory getBase() {
        if (null == base) {
            this.base = PageFactory.initElements(getDriver(), BasePageFactory.class);
        }
        return base;
    }

    public boolean noDataFound() {
        return getBase().getNoResultsFoundModal().size() > 0;
    }

    public void closeNoDataFoundAlert() {
        if (noDataFound()) {
            Click.element(getDriver(), getBase().getCloseModal());
            Loader.load(getDriver());
        }
    }

    public void close() {
        RetryUtilImpl.retry(5, new IRetryFunction() {
            @Override
            public void doThis() {
                new WebDriverWait(getDriver(), 5).until(
                        ExpectedConditions.elementToBeClickable(getBase().getCloseModal()))
                        .click();
                new WebDriverWait(getDriver(), 5).until(d -> !isOpen());
            }

            @Override
            public void logErrors(Exception e) {
                e.printStackTrace();
            }
        });
    }
}
