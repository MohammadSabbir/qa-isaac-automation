package com.api.modals;

import com.api.pagefactory.CreatePlayModalPageFactory;
import com.api.util.Click;
import com.api.util.IRetryFunction;
import com.api.util.Loader;
import com.api.util.RetryUtilImpl;
import com.google.common.base.Verify;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.*;

public class CreatePlayModal extends BaseModal {

    private final CreatePlayModalPageFactory page;

    public CreatePlayModal(WebDriver driver) {
        super(driver);
        this.page = PageFactory.initElements(getDriver(), CreatePlayModalPageFactory.class);
    }

    public CreatePlayModalPageFactory getPage() {
        return page;
    }

    @Override
    public boolean isOpen() {
        return getPage().getModalWindow().size() > 0;
    }

    @Override
    public void close() {

    }

    public String getPlayName() {
        return getPage().getPlayName().getAttribute("value");
    }

    public void setPlayName(String name) {
        RetryUtilImpl.retry(15, new IRetryFunction() {
            @Override
            public void doThis() {
                if (!getPlayName().equals(name)) {
                    getPage().getPlayName().clear();
                    getPage().getPlayName().sendKeys(name);
                    Loader.load(getDriver());
                }
                new WebDriverWait(getDriver(), 5).until(
                        ExpectedConditions.attributeToBe(
                                getPage().getPlayName(), "value", name)
                );
            }

            @Override
            public void logErrors(Exception e) {

            }
        });
    }

    public String getDescription() {
        return getPage().getDescription().getAttribute("innerText");
    }

    public void setDescription(String description) {
        RetryUtilImpl.retry(5, new IRetryFunction() {
            @Override
            public void doThis() {
                if (!getDescription().equals(description)) {
                    getPage().getDescription().clear();
                    getPage().getDescription().sendKeys(description);
                    Loader.load(getDriver());
                }
                new WebDriverWait(getDriver(), 5).until(
                        ExpectedConditions.textToBePresentInElement(
                                getPage().getDescription(), description)
                );
            }

            @Override
            public void logErrors(Exception e) {
            }
        });
    }

    public void savePlay() {
        ExecutorService executor = Executors.newFixedThreadPool(1);
        Future<Boolean> saveThePlay = executor.submit(() -> {
            while (isOpen()) {
                Click.element(getDriver(), getPage().getSaveButton());
                load();
            }
            return getPage().getSavedToaster().stream().anyMatch(WebElement::isDisplayed);
        });

        try {
            Verify.verify(saveThePlay.get(30, TimeUnit.SECONDS));
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }
    }
}
