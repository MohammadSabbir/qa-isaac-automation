package com.api.modals;

import com.api.pagefactory.ActionBarModalPageFactory;
import com.api.util.Click;
import com.api.util.IRetryFunction;
import com.api.util.Loader;
import com.api.util.RetryUtilImpl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.function.Supplier;

public class ActionBarModal {

    private final WebDriver driver;
    private ActionBarModalPageFactory page;
    private PPDModal ppdModalPF;
    private final Boolean playbookEnabled, ppdEnabled, paEnabled;

    public ActionBarModal(WebDriver driver) {
        this.driver = driver;
        this.playbookEnabled = isPlaybookEnabled();
        this.ppdEnabled = isPPDEnabled();
        this.paEnabled = isPhysicianAdvisorEnabled();
    }

    private WebDriver getDriver() {
        return driver;
    }

    private ActionBarModalPageFactory getPage() {
        if (null == page) {
            this.page = PageFactory.initElements(getDriver(), ActionBarModalPageFactory.class);
        }
        return page;
    }

    public boolean isPlaybookEnabled() {
        return !getPage().getAddToPlaybook().getAttribute("class")
                .toLowerCase().contains("disabled");
    }

    public boolean isPPDEnabled() {
        return !getPage().getAddToPPDButton().getAttribute("class")
                .toLowerCase().contains("disabled");
    }

    public boolean isPhysicianAdvisorEnabled() {
        return !getPage().getAddToPAButton().getAttribute("class")
                .toLowerCase().contains("disabled");
    }

    public PPDModal openPPD() {
        String x = "//*[@id='category']//*[contains(@class,'cgt-select-menu')]";
        Supplier<By> getXpath = () -> By.xpath(x);

        final PPDModal[] ppdModal = {null};
        if (isPPDEnabled()) {
            RetryUtilImpl.retry(5, new IRetryFunction() {
                @Override
                public void doThis() {
                    Click.element(getDriver(), getPage().getAddToPPDButton());
                    Loader.load(getDriver());
                    new WebDriverWait(getDriver(), 5).until(
                            ExpectedConditions.numberOfElementsToBeMoreThan(getXpath.get(), 0)
                    );
                    ppdModal[0] = new PPDModal(getDriver());
                }

                @Override
                public void logErrors(Exception e) {
                    e.printStackTrace();
                }
            });
        }
        return ppdModal[0];
    }

    public CreatePlayModal openPlaybook() {
        String x = "//*[@id='Modal' and contains(@class,'Playbook')]";
        Supplier<By> getXpath = () -> By.xpath(x);

        final CreatePlayModal[] cap = {null};
        if (isPPDEnabled()) {
            RetryUtilImpl.retry(5, new IRetryFunction() {
                @Override
                public void doThis() {
                    Click.element(getDriver(), getPage().getAddToPlaybook());
                    Loader.load(getDriver());
                    new WebDriverWait(getDriver(), 5).until(
                            ExpectedConditions.numberOfElementsToBeMoreThan(getXpath.get(), 0)
                    );
                    cap[0] = new CreatePlayModal(getDriver());
                }

                @Override
                public void logErrors(Exception e) {
                    e.printStackTrace();
                }
            });
        }
        return cap[0];
    }
}