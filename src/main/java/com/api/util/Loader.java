package com.api.util;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Supplier;

public class Loader {

    public static Function<WebDriver, Long> loader = (d) -> {
        long startTime = System.nanoTime();
        load(d);
        Supplier<List<WebElement>> elements = () -> d.findElements(
                By.xpath("//span[contains(@class,'warning-message') and contains(text(),'no results found')]"));

        Supplier<WebElement> closeModal = () -> d.findElement(By.xpath(
                "//*[@class='ui-dialog-buttonset']//button/span[contains(text(),'CLOSE')]"));

        while (elements.get().size() > 0) {
            Click.element(d, closeModal.get());
        }
        long endTime = System.nanoTime();
        return endTime - startTime;
    };

    private static final Function<WebDriver, Long> load = (d) -> {
        long startTime = System.nanoTime();
        Supplier<List<WebElement>> elements =
                () -> d.findElements(By.xpath("//div[@class='loader-svg']"));

        while ((elements.get()).size() > 0) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        new WebDriverWait(d, 360).until(ExpectedConditions
                .numberOfElementsToBe(By.xpath("//div[@class='loader-svg']"), 0));

        Supplier<List<WebElement>> closeModal = () -> d.findElements(By.xpath(
                "//*[@class='ui-dialog-buttonset']//button/span[contains(text(),'CLOSE')]"));

        if (closeModal.get().size() > 0) {
            Click.element(d, closeModal.get().get(0));
        }

        long endTime = System.nanoTime();
        return endTime - startTime;
    };

    /**
     * Waits for the ISAAC Spinner to disappear. Max time by default is 360
     * Seconds.
     */
    public static void load(WebDriver driver) {
        load.apply(driver);
    }
}
