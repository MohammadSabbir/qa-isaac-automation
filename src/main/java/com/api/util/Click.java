package com.api.util;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

public class Click {

    public static void element(WebDriver driver, WebElement element) {
        //Create our click interface
        Actions act = new Actions(driver);

        //A flag to control how many times we move to the element
        final boolean[] elementDisplayed = {false};

        RetryUtilImpl.retry(25, new IRetryFunction() {
            @Override
            public void doThis() {
                if (!elementDisplayed[0]) {
                    act.moveToElement(element).build().perform();
                    new WebDriverWait(driver, TimeUnit.SECONDS.toMillis(5)).until(
                            ExpectedConditions.visibilityOf(element));
                    elementDisplayed[0] = true;
                }
                new WebDriverWait(driver, TimeUnit.SECONDS.toMillis(5)).until(
                        ExpectedConditions.elementToBeClickable(element));
                act.moveToElement(element).click(element).build().perform();
            }

            @Override
            public void logErrors(Exception e) {
            }
        });
    }

    public static void element(WebDriver d, WebElement e, int retryAttempts, TimeUnit timeUnit,
                               Duration duration) {
        Actions act = new Actions(d);

        final boolean[] elementDisplayed = {false};
        RetryUtilImpl.retry(retryAttempts, new IRetryFunction() {
            @Override
            public void doThis() {
                if (!(elementDisplayed[0] = e.isDisplayed())) {
                    move(d, e, timeUnit, duration);
                    elementDisplayed[0] = new WebDriverWait(d, timeUnit.convert(duration)).until(
                            ExpectedConditions.visibilityOf(e)).isDisplayed();
                }
                new WebDriverWait(d, timeUnit.convert(duration)).until(
                        ExpectedConditions.elementToBeClickable(e));
                act.moveToElement(e).click(e).build().perform();
            }

            @Override
            public void logErrors(Exception e) {
                System.out.println("Failed to Click Element because " + e.getLocalizedMessage());
            }
        });
    }

    public static void elementUntilTrue(WebDriver d, WebElement e, Predicate<WebElement> elementPredicate) {
        RetryUtilImpl.retry(5, new IRetryFunction() {
            @Override
            public void doThis() {
                if (!elementPredicate.test(e)) {
                    move(d, e, TimeUnit.SECONDS, Duration.ofSeconds(3));
                    element(d, e, 3, TimeUnit.SECONDS, Duration.ofSeconds(5));
                    Loader.load(d);
                }
                Assert.assertTrue(elementPredicate.test(e));
            }

            @Override
            public void logErrors(Exception e) {
                System.out.println("Failed to click to element because " + e.getLocalizedMessage());
            }
        });
    }

    public static void move(WebDriver d, WebElement element) {
        //Create our click interface
        Actions act = new Actions(d);

        long startTime = System.currentTimeMillis();
        // Will loop for 10 seconds.
        while ((System.currentTimeMillis() - startTime) < 10000) {
            try {
                if (element.getSize().getHeight() > 0) {
                    act.moveToElement(element).build().perform();
                    break;
                }
            } catch (Exception e) {
                System.out.println("Failed to move to element because " + e.getLocalizedMessage());
            }
        }
    }

    public static void move(WebDriver d, WebElement e, TimeUnit timeUnit, Duration duration) {
        Actions act = new Actions(d);
        long startTime = System.currentTimeMillis();

        while ((System.currentTimeMillis() - startTime) < timeUnit.convert(duration)) {
            try {
                if (e.getSize().getHeight() > 0) {
                    act.moveToElement(e).build().perform();
                    break;
                }
            } catch (Exception x) {
                System.out.println("Failed to move to element because " + x.getLocalizedMessage());
            }
        }
    }
}
