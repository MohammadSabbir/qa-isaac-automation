package com.api.util;

public class MathUtils {

    public static double percent(double a, double b) {
        return ((a - b) / (a)) * 100;
    }
}
