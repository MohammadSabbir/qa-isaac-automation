/*
 * (c) Cedargate Technologies
 */

package com.api.util;

public final class RetryUtilImpl {


    public static void retry(int timesToRetry, IRetryFunction function) {
        for (int i = timesToRetry; i > -1; i--) {
            try {
                function.doThis();
                break;
            } catch (Exception e) {
                function.logErrors(e);
            }
        }
    }
}
