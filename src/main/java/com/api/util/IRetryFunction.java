/*
 * (c) Cedargate Technologies
 */

package com.api.util;

public interface IRetryFunction {

    void doThis();

    void logErrors(Exception e);
}
