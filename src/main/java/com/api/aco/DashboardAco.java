package com.api.aco;

import com.api.pages.Dashboard;
import com.api.pagefactory.DashboardPageFactory;
import com.api.util.Click;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class DashboardAco extends Aco<DashboardAco> {

    public DashboardAco(String aco, int index) {
        super(aco, index);
    }

    public String getTotalOpportunity(WebDriver d) {
        var page = PageFactory.initElements(d, DashboardPageFactory.class);
        try {
            return page.getAllTotalOpportunity().getAttribute("innerText");
        } catch (Exception e) {
            return null;
        }
    }

    public Double getTotalOpportunityNumber(WebDriver d) {
        var page = PageFactory.initElements(d, DashboardPageFactory.class);

        String da = null;
        try {
            da = page.getAllTotalOpportunity().getAttribute("data-amount");
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (null != da) {
            return Double.parseDouble(da);
        }
        else {
            return 0.0;
        }
    }

    public void openDropdown(WebDriver d) {
        var page = PageFactory.initElements(d, DashboardPageFactory.class);
        page.getContractArrows().stream().filter(e ->
                e.getAttribute("id").equals(String.format("contract-%s", getAco())))
                    .forEach(element -> Click.elementUntilTrue(d, element, e1 -> e1.getAttribute("class").contains("open")));
    }

    public void selectAco(WebDriver d) {
        if (getAco().equalsIgnoreCase("all")) {
            return;
        }

        var page = PageFactory.initElements(d, DashboardPageFactory.class);
        new WebDriverWait(d, 15).until(d1 -> page.getContracts().size() > 0);

        Dashboard dashboard = new Dashboard(d);
        List<DashboardAco> parents = new ArrayList<>();
        var root = dashboard.getAco(getAco());
        while (root.hasParent()) {
            root = dashboard.getAco(root.getParent());
            parents.add(root);
        }

        for (int i = parents.size() - 1; i >= 0; i--) {
            parents.get(i).openDropdown(d);
        }

        Click.elementUntilTrue(d, page.getContracts().get(getIndex()),
                e -> e.getAttribute("class").contains("selected"));
    }

    public double getAnnualSpend(WebDriver d) {
        var page = PageFactory.initElements(d, DashboardPageFactory.class);
        return Double.parseDouble(page.getContractAnnualSpends().get(getIndex()).getAttribute("data-amount"));
    }

    public WebElement getElement(WebDriver d) {
        var page = PageFactory.initElements(d, DashboardPageFactory.class);
        return page.getContracts().get(getIndex());
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public double getLeakageOpportunity(WebDriver d) {
        var page = PageFactory.initElements(d, DashboardPageFactory.class);
        return Double.parseDouble(page.getContractLeakageOpportunities().get(getIndex()).getAttribute("data-amount"));
    }

    public double getValue(WebDriver d) {
        return getLeakageOpportunity(d) + getAnnualSpend(d);
    }

    public boolean isRbacDisabled(WebDriver d) {
        return getElement(d).getAttribute("class").contains("has-no-click");
    }

    public boolean isClickable(WebDriver d) {
        return checkAgainstElement(d, getElement(d), e1 -> !e1.getAttribute("class").contains("has-no-click"));
    }

    public boolean isSelected(WebDriver d) {
        return checkAgainstElement(d, getElement(d), e1 -> e1.getAttribute("class").contains("selected"));
    }

    private boolean checkAgainstElement(WebDriver d, WebElement e, Predicate<WebElement> elementPredicate) {
        return elementPredicate.test(e);
    }
}
