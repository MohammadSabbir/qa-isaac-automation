package com.api.aco;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import java.util.stream.IntStream;

public class AcoGraph<T extends Aco<T>> {

    private final List<T> acoList;
    private final Class<T> clazz;
    private final List<T> roots;
    private final LinkedList<T> orderedAcoList;

    public AcoGraph(Class<T> clazz, List<T> acoList) {
        this.clazz = clazz;
        this.acoList = acoList;
        this.roots = new ArrayList<>();
        buildAcoGraph();
        this.orderedAcoList = orderAllAcos();
    }

    private T buildOne(String aco, int index) {
        try {
            Constructor<T> constructor = clazz.getConstructor(String.class, Integer.TYPE);
            return constructor.newInstance(aco, index);
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    private void buildAcoGraph() {
        for (T aco : acoList) {
            if (null == getAco(aco.getAco())) {
                addToRoots(aco);
            }
        }
    }

    private void addToRoots(T aco) {
        T rootNode;
        if (!aco.hasParent()) {
            rootNode = buildOne(aco.getAco(), aco.getIndex());
            roots.add(rootNode);
        } else {
            long count = aco.getAco().chars().filter(ch -> ch == '_').count();
            String[] split = aco.getAco().split("_");

            if (count == 1) { //Level 1 Bene for example MSSP_ABC
                String lvl0 = split[0];
                String lvl1 = split[0] + "_" + split[1];

                rootNode = roots.stream().filter(e -> e.getAco().equals(lvl0))
                        .findFirst().orElseThrow();

                T lvl1Node = rootNode.getChildren().stream().filter(e -> e.getAco().equals(lvl1))
                        .findFirst().orElse(null);

                if (null == lvl1Node) {
                    //Add to Root
                    int indexOfRoot = IntStream.range(0, roots.size())
                            .filter(e -> roots.get(e).getAco().equals(lvl0))
                            .findFirst().orElse(9999);

                    lvl1Node = buildOne(aco.getAco(), aco.getIndex());
                    roots.get(indexOfRoot).addChild(lvl1Node);
                }


            } else if (count == 2) { //Level 2 Bene for example MSSP_ABC_ABC
                String lvl1 = split[0] + "_" + split[1];
                String lvl0 = split[0];
                String lvl2 = split[0] + "_" + split[1] + "_" + split[2];

                int indexInRoots = IntStream.range(0, roots.size()).filter(r -> roots.get(r).getAco().equals(lvl0)).findFirst()
                        .orElseThrow();

                Vector<T> children = roots.get(indexInRoots).getChildren();
                int indexInChildren = IntStream.range(0, children.size())
                        .filter(e -> children.get(e).getAco().equals(lvl1))
                            .findFirst().orElseThrow();

                T lvl1Node = roots.get(indexInRoots).getChildren().get(indexInChildren);

                T lvl2Node = lvl1Node.getChildren().stream().filter(e -> e.getAco().equals(lvl2))
                        .findFirst().orElse(null);

                if (null == lvl2Node) {
                    lvl2Node = buildOne(aco.getAco(), aco.getIndex());
                    roots.get(indexInRoots).getChildren().get(indexInChildren).addChild(lvl2Node);
                }
            }
        }
    }

    public T getAco(String aco) {
        StringBuilder lvl = new StringBuilder();
        T node = null;
        for (String s : aco.split("_")) {

            if (lvl.length() == 0) {
                lvl.append(s);
            } else {
                lvl.append("_").append(s);
            }

            if (null == node) {
                node = roots.stream().filter(root -> root.getAco().equals(lvl.toString()))
                        .findFirst().orElse(null);

                if (null == node) {
                    return null;
                }
            }

            if (!lvl.toString().equals(node.getAco())) {
                node = node.getChildren().stream().filter(e -> e.getAco().equals(lvl.toString()))
                        .findFirst().orElse(null);

                if (null == node) {
                    return null;
                }
            }
        }
        return node;
    }


    private LinkedList<T> orderAllAcos() {
        LinkedList<T> orderedAcoList = new LinkedList<>();
        for (T root : roots) {
            orderedAcoList.add(root);

            //Add Sub Children
            if (root.hasChildren()) {
                for (T child : root.getChildren()) {
                    orderedAcoList.add(child);

                    //Add Child Child
                    if (child.hasChildren()) {
                        orderedAcoList.addAll(child.getChildren());
                    }
                }
            }
        }
        return orderedAcoList;
    }

    public LinkedList<T> getAcoList() {
        if (orderedAcoList.size() == 0) {
            orderAllAcos();
        }
        return orderedAcoList;
    }

    public List<T> getLevel0s() {
        return roots;
    }

    public List<T> getLevel1() {
        List<T> level1s = new ArrayList<>();
        getLevel0s().stream().map(Aco::getChildren).forEach(level1s::addAll);
        return level1s;
    }

    public List<T> getLevel2() {
        List<T> level2s = new ArrayList<>();
        getLevel1().stream().map(Aco::getChildren).forEach(level2s::addAll);
        return level2s;
    }

    public List<T> getLevel0() {
        List<T> level0s = new ArrayList<>();
        getLevel1().stream().filter(e -> !e.hasParent()).forEach(level0s::add);
        return level0s;
    }
}
