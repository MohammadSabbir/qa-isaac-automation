package com.api.aco;

import java.util.Vector;

public class Aco<T> {

    private final String aco;
    private final int index;
    private final Vector<T> children;


    public Aco(String aco, Integer index) {
        this.aco = aco;
        this.index = index;
        this.children = new Vector<>();
    }

    public int getIndex() {
        return index;
    }

    public String getAco() {
        return aco;
    }


    public boolean hasParent() {
        long count = getAco().chars().filter(e -> e == '_').count();
        return count > 0;
    }

    public String getParent() {
        StringBuilder parent = new StringBuilder();
        if (hasParent()) {
            String[] acos = getAco().split("_");
            for (int i = acos.length - 2; i >= 0; i--) {
                if (parent.length() == 0) {
                    parent.append(acos[i]);
                } else {
                    parent.insert(0, "_").insert(0, acos[i]);
                }
            }
        }
        return parent.toString().isEmpty() ? null : parent.toString();
    }

    public <C extends T> void addChild(C child) {
        children.addElement(child);
    }

    public Vector<T> getChildren() {
        return children;
    }

    public Boolean hasChildren() {
        return getChildren().size() > 0;
    }

    @Override
    public String toString() {
        return "Aco2{" +
                "aco='" + aco + '\'' +
                ", index=" + index +
                ", children=" + children +
                '}';
    }
}
