package com.api.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class PPDModalPageFactory extends BasePageFactory {

    @FindBy(xpath = "//*[@class='modal-body-cnt physician-performance-wrapper']")
    private List<WebElement> modalWindow = null;

    @FindBy(xpath = "//*[@id='ppd-performance-button']//a")
    private List<WebElement> MemberDetailButton = null;

    @FindBy(xpath = "//*[@id='Modal']//*[@id='contractName']")
    private WebElement modalTitle = null;

    @FindBy(xpath="//*[@id='ppd-opportunity-details']//a")
    private List<WebElement> opportunityBarcharts = null;

    @FindBy(xpath = "//*[@id='ppd-clinical-pathway-barChart']//a")
    private List<WebElement> Pathways = null;

    @FindBy(xpath = "//*[@id='category']//*[contains(@class,'cgt-select-menu')]")
    private WebElement categoryDropdown = null;

    @FindBy(xpath = "//*[@id='category']//*[contains(@class,'cgt-select-menu')]//div[@class='select-label']")
    private WebElement categoryDropdownLabel = null;

    @FindBy(xpath = "//*[@id='category']//*[contains(@class,'select-list thin-scroll active')]//div")
    private List<WebElement> CategoryDropdownOptions = null;

    public PPDModalPageFactory(WebDriver driver) {
        super(driver);
    }

    public List<WebElement> getModalWindow() {
        return modalWindow;
    }

    public List<WebElement> getMemberDetailButton() {
        return MemberDetailButton;
    }

    public WebElement getModalTitle() {
        return modalTitle;
    }

    public List<WebElement> getOpportunityBarcharts() {
        return opportunityBarcharts;
    }

    public List<WebElement> getPathways() {
        return Pathways;
    }

    public WebElement getCategoryDropdown() {
        return categoryDropdown;
    }

    public WebElement getCategoryDropdownLabel() {
        return categoryDropdownLabel;
    }

    public List<WebElement> getCategoryDropdownOptions() {
        return CategoryDropdownOptions;
    }
}
