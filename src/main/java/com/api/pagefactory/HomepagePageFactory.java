package com.api.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class HomepagePageFactory {

    private final WebDriver driver;

    public HomepagePageFactory(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(xpath = "//input[@id='username']")
    private WebElement usernameInput = null;

    @FindBy(xpath = "//input[@id='username']")
    private List<WebElement> usernameList = null;

    @FindBy(xpath = "//input[@id='password']")
    private WebElement passwordInput = null;

    @FindBy(xpath = "//button[@id='submitButton']")
    private WebElement submit = null;

    public WebDriver getDriver() {
        return driver;
    }

    public WebElement getUsernameInput() {
        return usernameInput;
    }

    public List<WebElement> getUsernameList() {
        return usernameList;
    }

    public WebElement getPasswordInput() {
        return passwordInput;
    }

    public WebElement getSubmit() {
        return submit;
    }
}
