package com.api.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class PatientVolumeModalPageFactory extends BasePageFactory {

    @FindBy(xpath = "//*[@id='MinEpisode']//*[contains(@class,'dropdown-menu')]//*[@class='save']")
    private WebElement saveButton = null;
    @FindBy(xpath = "//*[@id='MinEpisode']//*[@class='dropdown-menu active']//*[@role='slider' and contains(@class,'2')]")
    private List<WebElement> maxButton = null;
    @FindBy(xpath = "//*[@data-clickedon='min-episodes']//*[contains(@class,'rc-slider-mark-text')]")
    private List<WebElement> marks = null;
    @FindBy(xpath = "//*[@class='rc-slider-step']//span")
    private List<WebElement> physicialMark = null;
    @FindBy(xpath = "//*[@id='MinEpisode']")
    private WebElement dropdown = null;
    @FindBy(xpath = "//*[@data-clickedon='min-episodes' and contains(@class,'dropdown') and contains(@class,'active')]")
    private List<WebElement> dropdownOpen = null;
    @FindBy(xpath = "//*[@id='MinEpisode']//div[contains(@class,'dropdown-menu')]")
    private WebElement panel = null;
    @FindBy(xpath = "//*[@data-clickedon='min-episodes' and @data-savings]//span[@data-amount]")
    private WebElement savings = null;
    @FindBy(xpath = "//*[@role='slider' and contains(@class,'rc-slider-handle rc-slider-handle-1')]")
    private WebElement min = null;
    @FindBy(xpath = "//*[@id='MinEpisode']//*[contains(@class,'dropdown-menu')]//*[@class='save']")
    private WebElement save = null;
    @FindBy(xpath = "//*[@id='MinEpisode']//*[@class='dropdown-menu active']//*[@role='slider' and contains(@class,'2')]")
    private WebElement max = null;

    public PatientVolumeModalPageFactory(WebDriver driver) {
        super(driver);
    }

    public WebElement getSaveButton() {
        return saveButton;
    }

    public List<WebElement> getMaxButton() {
        return maxButton;
    }

    public WebElement getMax() {
        return max;
    }

    public WebElement getMin() {
        return min;
    }

    public WebElement getSave() {
        return save;
    }

    public List<WebElement> getDropdownOpen() {
        return dropdownOpen;
    }

    public List<WebElement> getMarks() {
        return marks;
    }

    public List<WebElement> getPhysicialMark() {
        return physicialMark;
    }

    public WebElement getDropdown() {
        return dropdown;
    }

    public WebElement getPanel() {
        return panel;
    }

    public WebElement getSavings() {
        return savings;
    }
}
