package com.api.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ClinicalPageFactory extends SavingsAdvisorPageFactory {

    @FindBy(xpath = "//*[@id='cv-savings-opportunity-by-type']//ol//li//a")
    private List<WebElement> clinicalVariations = null;

    public ClinicalPageFactory(WebDriver d) {
        super(d);
    }

    public List<WebElement> getClinicalVariations() {
        return clinicalVariations;
    }
}
