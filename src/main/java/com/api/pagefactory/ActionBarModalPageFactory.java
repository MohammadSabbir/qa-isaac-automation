package com.api.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ActionBarModalPageFactory extends BasePageFactory {

    @FindBy(xpath = "//*[@id='playbookBox']//*[@id='openPP']")
    private WebElement addToPPDButton = null;

    @FindBy(xpath = "//*[@id='playbookBox']//*[@id='gotoPA']")
    private WebElement addToPAButton = null;

    @FindBy(xpath = "//*[@id='playbookBox']//*[@id='addToPlaybook']")
    private WebElement addToPlaybook = null;

    @FindBy(xpath = "//*[@id='AddToPlaybookView']//*[@id='NWPlaybookAmount']//span")
    private WebElement totalOpportunity = null;

    @FindBy(xpath = "//*[@id='Modal']//*[@id='contractName']")
    private WebElement modalTitle = null;

    public ActionBarModalPageFactory(WebDriver driver) {
        super(driver);
    }

    public WebElement getAddToPPDButton() {
        return addToPPDButton;
    }

    public WebElement getAddToPAButton() {
        return addToPAButton;
    }

    public WebElement getAddToPlaybook() {
        return addToPlaybook;
    }

    public WebElement getModalTitle() {
        return modalTitle;
    }

    public WebElement getTotalOpportunity() {
        return totalOpportunity;
    }
}
