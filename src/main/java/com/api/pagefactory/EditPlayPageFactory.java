package com.api.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class EditPlayPageFactory extends BasePageFactory {

    @FindBy(xpath = "//tr[contains(@id,'play-id')]")
    private List<WebElement> plays = null;

    @FindBy(xpath = "//tr[contains(@id,'play-id')]//td[contains(@class,'playName')]//a")
    private List<WebElement> playNames = null;

    public EditPlayPageFactory(WebDriver driver) {
        super(driver);
    }

    public List<WebElement> getPlays() {
        return plays;
    }

    public List<WebElement> getPlayNames() {
        return playNames;
    }
}
