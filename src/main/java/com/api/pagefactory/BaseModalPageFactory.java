package com.api.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public abstract class BaseModalPageFactory {

    private final WebDriver driver;
    @FindBy(xpath = "//*[contains(@id,'close-modal')]")
    private WebElement closeModal = null;

    public BaseModalPageFactory(WebDriver driver) {
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public WebElement getCloseModal() {
        return closeModal;
    }
}
