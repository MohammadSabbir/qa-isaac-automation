package com.api.pagefactory;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class DashboardPageFactory {

    @FindBy(xpath = "//*[@class='tbody' or @class='contract-sublist']/label")
    private List<WebElement> contracts = null;

    @FindBy(xpath = "//*[@class='tbody' or @class='contract-sublist']/label/span[@class ='name-cnt']")
    private List<WebElement> contractNames = null;

    @FindBy(xpath = "//*[@class='tbody' or @class='contract-sublist']/label/span[@class ='savings-cnt']//*[@class='amount']")
    private List<WebElement> contractAnnualSpends = null;

    @FindBy(xpath = "//*[@class='tbody' or @class='contract-sublist']/label/span[@class ='leakage-cnt']//*[@data-amount]")
    private List<WebElement> contractLeakageOpportunities = null;

    @FindBy(xpath = "//*[@class='tbody']//*[contains(@class,'icon')]")
    private List<WebElement> contractArrows = null;

    @FindBy(xpath = "//*[@class='opportunity-tile']//*[@class='amt']")
    private WebElement allTotalOpportunity = null;

    @FindBy(xpath ="//*[@class='opportunity-tile']//span[@class='amt']")
    private WebElement dashboardTotalOpportunity = null;

    @FindBy(xpath = "//*[@class='opportunity-tile'] |  //*[contains(@class,'opportunity-tile')]//h3[contains(text(),':')] | //*[contains(@class,'opportunity-tile')]//div[@class='chicklet-cnt']")
    private List<WebElement> ChicletsAndOpportunities = null;

    public List<WebElement> getChicletsAndOpportunities() {
        return ChicletsAndOpportunities;
    }

    public List<WebElement> getContracts() {
        return contracts;
    }

    public List<WebElement> getContractNames() {
        return contractNames;
    }

    public List<WebElement> getContractArrows() {
        return contractArrows;
    }

    public WebElement getAllTotalOpportunity() {
        return allTotalOpportunity;
    }

    public List<WebElement> getContractAnnualSpends() {
        return contractAnnualSpends;
    }

    public List<WebElement> getContractLeakageOpportunities() {
        return contractLeakageOpportunities;
    }

    public WebElement getDashboardTotalOpportunity() {
        return dashboardTotalOpportunity;
    }
}
