package com.api.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public abstract class BasePageFactory {

    private final WebDriver driver;

    @FindBy(xpath = "//*[@id='confirmNavigationAwayDiv']//*[contains(text(),'No Results Found')]")
    private List<WebElement> NoResultsFoundModal = null;
    @FindBy(xpath = "//*[@id='ClientLogo']//img")
    private List<WebElement> clientLogos = null;
    @FindBy(xpath = "//*[@id='global-advisor-header']")
    private WebElement advisorHeaderLabel = null;
    @FindBy(xpath = "//*[@id='print-user-info']")
    private WebElement userInfo = null;
    @FindBy(xpath = "//*[contains(@class,'close-modal')]")
    private WebElement closeModal = null;

    public BasePageFactory(WebDriver driver) {
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public List<WebElement> getNoResultsFoundModal() {
        return NoResultsFoundModal;
    }

    public List<WebElement> getClientLogos() {
        return clientLogos;
    }

    public WebElement getAdvisorHeaderLabel() {
        return advisorHeaderLabel;
    }

    public WebElement getUserInfo() {
        return userInfo;
    }

    public WebElement getCloseModal() {
        return closeModal;
    }
}
