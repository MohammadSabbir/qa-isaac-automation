package com.api.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Elements shared across all advisors.
 */
public class NavigationMenuPageFactory extends BasePageFactory {

    @FindBy(xpath = "//*[@id='outerTabs']//*[contains(@class,'tab')]")
    private List<WebElement> navigationMenuTabButtons = null;
    @FindBy(xpath = "//*[@id='executiveDashboard']")
    private WebElement dashboardTab = null;
    @FindBy(xpath = "//*[@id='playbookPerformance']")
    private WebElement pbPerformanceDropdownOption = null;
    @FindBy(xpath = "//*[@id='playbookBundles']")
    private WebElement pbBundlesDropdownOption = null;
    @FindBy(xpath = "//*[@id='dd-label' and contains(text(),'Advisors')]")
    private WebElement AdvisorsTab = null;
    @FindBy(xpath = "//*[@id='outerTabs']//*[(@data-advisor) and @data-advisor='NRA']")
    private WebElement networkDropdownOption = null;
    @FindBy(xpath = "//*[@id='outerTabs']//*[(@data-advisor) and @data-advisor='IA']")
    private WebElement inpatientDropdownOption = null;
    @FindBy(xpath = "//*[@id='outerTabs']//*[(@data-advisor) and @data-advisor='CV']")
    private WebElement clinicalDropdownOption = null;
    @FindBy(xpath = "//*[@id='outerTabs']//*[(@data-advisor) and @data-advisor='PA']")
    private WebElement physicianDropdownOption = null;
    @FindBy(xpath = "//*[(@id and @data-advisor) or @class='dropdown' or @class='isDDList no-hover']")
    private List<WebElement> allOptionsOnNavDropdown = null;
    @FindBy(xpath = "//*[@id='outerTabs']//*[(@data-advisor) and @data-advisor='LP']")
    private WebElement leakagePhysicianDropdownOption = null;
    @FindBy(xpath = "//*[@id='outerTabs']//*[(@data-advisor) and @data-advisor='LA']")
    private WebElement leakageFacilityDropdownOption = null;
    @FindBy(xpath = "//*[@id='outerTabs']//*[(@data-advisor) and @id='playbookSavings']")
    private WebElement PlaybookSavingsDropdownOption = null;
    @FindBy(xpath = "//*[@id='outerTabs']//*[(@data-advisor) and @id='playbookSavings_Revenue']")
    private WebElement playbookLeakageDropdownOption = null;
    @FindBy(xpath = "//*[@id='prescriptiveAdvisor']")
    private WebElement MakePlayPerformanceDropdownOption = null;
    @FindBy(xpath = "//*[@id='bundlesAdvisor']")
    private WebElement MakePlayPerformanceBundlesDropdownOption= null;
    @FindBy(xpath = "//*[@id='outerTabs']//*[contains(@class,'isDDList no-hover') or (contains(@class,'dropdown') and not(contains(@class,'content')))]")
    private List<WebElement> dropdownOptions = null;
    @FindBy(xpath = "//*[@id='global-advisor-header']")
    private WebElement currentAdvisor = null;
    @FindBy(xpath = "(//a[@class='isDDList no-hover']//span[//div[@class='sub']])[1]")
    private WebElement leakageDropdownOption = null;
    @FindBy(xpath = "//*[@id='dd-label' and contains(text(),'make a play')]")
    private WebElement makePlayDropdownOption = null;
    @FindBy(xpath = "//*[@id='dd-label' and contains(text(),'Playbook')]")
    private WebElement playbookDropdownOption = null;

    public NavigationMenuPageFactory(WebDriver driver) {
        super(driver);
    }

    public List<WebElement> getAllOptionsOnNavDropdown() {
        return allOptionsOnNavDropdown;
    }

    public WebElement getPlaybookDropdownOption() {
        return playbookDropdownOption;
    }

    public WebElement getAdvisorsTab() {
        return AdvisorsTab;
    }

    public List<WebElement> getNavigationMenuTabButtons() {
        return navigationMenuTabButtons;
    }

    public WebElement getPbBundlesDropdownOption() {
        return pbBundlesDropdownOption;
    }

    public WebElement getPbPerformanceDropdownOption() {
        return pbPerformanceDropdownOption;
    }

    public WebElement getDashboardTab() {
        return dashboardTab;
    }

    public WebElement getNetworkDropdownOption() {
        return networkDropdownOption;
    }

    public WebElement getInpatientDropdownOption() {
        return inpatientDropdownOption;
    }

    public WebElement getClinicalDropdownOption() {
        return clinicalDropdownOption;
    }

    public WebElement getMakePlayDropdownOption() {
        return makePlayDropdownOption;
    }

    public WebElement getMakePlayPerformanceBundlesDropdownOption() {
        return MakePlayPerformanceBundlesDropdownOption;
    }

    public WebElement getPhysicianDropdownOption() {
        return physicianDropdownOption;
    }

    public WebElement getLeakagePhysicianDropdownOption() {
        return leakagePhysicianDropdownOption;
    }

    public WebElement getLeakageFacilityDropdownOption() {
        return leakageFacilityDropdownOption;
    }

    public WebElement getPlaybookSavingsDropdownOption() {
        return PlaybookSavingsDropdownOption;
    }

    public WebElement getPlaybookLeakageDropdownOption() {
        return playbookLeakageDropdownOption;
    }

    public WebElement getMakePlayPerformanceDropdownOption() {
        return MakePlayPerformanceDropdownOption;
    }

    public WebElement getLeakageDropdownOption() {
        return leakageDropdownOption;
    }

    public List<WebElement> getDropdownOptions() {
        return dropdownOptions;
    }

    public WebElement getCurrentAdvisor() {
        return currentAdvisor;
    }

}
