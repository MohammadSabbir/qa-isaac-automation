package com.api.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class EditPlaySummaryPageFactory extends BasePageFactory {

    @FindBy(xpath = "//*[@class='form-buttons']//*[@value='Delete']")
    private WebElement deleteButton = null;

    @FindBy(xpath = "//*[contains(@class,'modal') and contains(@class,'delete')]//*[@value='OK']")
    private List<WebElement> deleteConfirmationButton = null;

    public EditPlaySummaryPageFactory(WebDriver driver) {
        super(driver);
    }

    public List<WebElement> getDeleteConfirmationButton() {
        return deleteConfirmationButton;
    }

    public WebElement getDeleteButton() {
        return deleteButton;
    }
}
