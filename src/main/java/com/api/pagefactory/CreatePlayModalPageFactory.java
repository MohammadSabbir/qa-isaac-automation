package com.api.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class CreatePlayModalPageFactory extends BaseModalPageFactory {

    public CreatePlayModalPageFactory(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[@id='Modal' and contains(@class,'Playbook')]")
    private List<WebElement> modalWindow = null;
    @FindBy(xpath = "//*[@class='physician-selector physician-selector-attribs']//h1//*[contains(@class,'name')]")
    private List<WebElement> physcianNames = null;
    @FindBy(xpath = "//*[@class='physician-selector physician-selector-attribs']//h1//*[contains(@class,'amount')]")
    private List<WebElement> PhysicianAmount = null;
    @FindBy(xpath = "//*[@class='physician-selector physician-selector-attribs']//h1//*[contains(@class,'conflicts')]")
    private List<WebElement> PhyscianConflict = null;
    @FindBy(xpath = "//*[@class='tile-cnt contracts']//li")
    private List<WebElement> Contracts = null;
    @FindBy(xpath = "//*[@id='Modal']//*[@class='tile-cnt physicians']//*[@class='barchart w-paginator hide']//li//a[@class='remove']")
    private List<WebElement> PhysiciansRemove = null;
    @FindBy(xpath = "//*[@class='conflict-cnt']")
    private List<WebElement> Conflicts = null;
    @FindBy(xpath = "//*[@class='modal PlaybookTargetModal add-to-playbook savings-screen']//*[@class='close-modal ']")
    private WebElement closeModal = null;
    @FindBy(xpath = "//*[@class='playname-cnt']//textarea")
    private WebElement Description = null;
    @FindBy(xpath = "(//*[@class='assigned-to-cnt'])//*[@id='assignedToOptions']/div")
    private List<WebElement> assigneeOptions = null;
    @FindBy(xpath = "//*[@id='Modal' and contains(@class,'playbook')]")
    private WebElement modalHeader = null;
    @FindBy(xpath = "(//*[@class='assigned-to-cnt'])/div")
    private WebElement assigneeDropdown = null;
    @FindBy(xpath = "//*[@role='slider']//span")
    private List<WebElement> DateSliderLabels = null;
    @FindBy(xpath = "//*[@role='slider']")
    private List<WebElement> DateSlider = null;
    @FindBy(xpath = "//*[@class='calculate-cnt']//h2")
    private WebElement CalculateSavingsOpportunityTargetLabel = null;
    @FindBy(xpath = "//input[@class='to-playbook']")
    private WebElement CaptureRate = null;
    @FindBy(xpath = "(//*[@class='calculate']//*[@class='input noselect'])[1]")
    private WebElement TotalOpportunity = null;
    @FindBy(xpath = "(//*[@class='calculate']//*[@class='input noselect'])[2]")
    private WebElement SavingsGoal = null;
    @FindBy(xpath = "//*[contains(@class,'playbook')]//*[@value ='Save']")
    private WebElement saveButton = null;
    @FindBy(xpath = "//*[@type='text' and contains(@placeholder,'Enter')]")
    private WebElement PlayName = null;
    @FindBy(xpath = "//*[@id='notification-prompt']/span/strong")
    private List<WebElement> SavedToaster = null;
    @FindBy(xpath = "//*[@disabled='' and @value ='Save']")
    private List<WebElement> SaveDisabled = null;
    @FindBy(xpath = "//*[@id='error-on-name']")
    private WebElement PlayName_Error = null;
    @FindBy(xpath = "//*[@class='assigned-to']")
    private WebElement TargetGroupDropdown = null;
    @FindBy(xpath = "//*[@class='calculate']//*[contains(text(),'total opportunity')]")
    private WebElement TotalOpportunityLabel = null;
    @FindBy(xpath = "//*[@class='calculate']//*[contains(text(),'Expected Capture Rate')]")
    private WebElement ExpectedCaptureRateLabel = null;
    @FindBy(xpath = "//*[@class='calculate']//*[contains(text(),'Savings')]")
    private WebElement SavingsGoalLabel = null;

    public List<WebElement> getModalWindow() {
        return modalWindow;
    }

    public WebElement getPlayName() {
        return PlayName;
    }

    public WebElement getDescription() {
        return Description;
    }

    public WebElement getSaveButton() {
        return saveButton;
    }

    public List<WebElement> getSavedToaster() {
        return SavedToaster;
    }
}
