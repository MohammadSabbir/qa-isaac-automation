package com.api.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SavingsAdvisorPageFactory extends BasePageFactory {

    @FindBy(xpath = "//*[contains(@id,'model-conditions')]/ol/li/a")
    private List<WebElement> Pathways = null;

    @FindBy(xpath = "//*[contains(@class,'attrib')]//div/ol/li/a")
    private List<WebElement> AttributedPhysicians = null;

    public SavingsAdvisorPageFactory(WebDriver d) {
        super(d);
    }

    public List<WebElement> getPathways() {
        return Pathways;
    }

    public List<WebElement> getAttributedPhysicians() {
        return AttributedPhysicians;
    }
}
