package com.automation.tests.smoke;

import com.automation.dataprovider.ISAACDataProvider;
import com.automation.tests.base.DemoTest;
import com.automation.tests.base.DemoTestTwo;
import com.automation.tests.carelocation.CareLocationTests;
import com.automation.tests.clinical.ClinicalTests;
import com.automation.tests.dashboard.DashboardTests;
import org.testng.annotations.Factory;

public class SmokeFactory {

    @Factory(dataProviderClass = ISAACDataProvider.class,
            dataProvider = ISAACDataProvider.ALL_CLIENTS, enabled = false)
    public Object[] runSingleTests(String clientId) {
        Object[] tests = new Object[]
                {
                        new DashboardTests(Integer.parseInt(clientId))
                };
        return tests;
    }

    @Factory(dataProviderClass = ISAACDataProvider.class, dataProvider = ISAACDataProvider.ALL_CONTRACTS,
    enabled = true)
    public Object[] runSavingsTests(int clientId, String aco) {
        Object[] tests = new Object[]
                {
                        new CareLocationTests(clientId, aco)
                };
        return tests;
    }
}
