package com.automation.tests.clinical;

import com.api.navigation.NavMenu;
import com.api.pages.ClinicalPage;
import com.api.pages.EditPlayModal;
import com.api.pages.EditPlaySummary;
import com.automation.tests.base.BaseTest;
import com.google.common.base.Verify;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class ClinicalTests extends BaseTest {

    private ClinicalPage clinical;
    private List<String> playNames;

    public ClinicalTests(int clientId, String aco) {
        super(clientId, aco);
    }

    @Test(groups = {"clinical"})
    public void clinicalTests() {
        this.clinical = new ClinicalPage(getDriver());
        NavMenu.CLINICAL.navigate(getDriver());
        validateHeader();
        validatePPD();
        validateSavingsPlay();
        deleteCreatedPlays();
    }

    @Test(groups = {"clinical.header"}, enabled = false)
    public void validateHeader() {
        boolean headerMatch = this.clinical.getHeader().equalsIgnoreCase("clinical advisor");
        boolean logoDisplayed = this.clinical.clientLogoDisplayed();
        Verify.verify(headerMatch && logoDisplayed,
                String.format("{tc: validateHeader, header-match: %s, advisor: cv}, {tc: validateClientLogo, logo-displayed:%s}"
                        , headerMatch, logoDisplayed));

    }

    @Test(enabled = false, groups = {"clinical.ppd"})
    public void validatePPD() {
        for (int i = 0; i < clinical.getNumberOfVariations(); i++) {
            clinical.selectVariation(i);

            if (clinical.getNumberOfPathways() == 0) {
                return;
            }
            clinical.selectPathway(0);


            if (clinical.getNumberOfAttributedPhysicians() == 0) {
                return;
            }
            clinical.selectAttributedPhysician(0);

            /*Open PPD*/
            var ppd = clinical.getActionBarModal().openPPD();
            Verify.verify(ppd.isOpen());
            if (ppd.getOpportunitySize() == 0) {
                return;
            }
            ppd.selectOpportunityDetail(0);

            if (ppd.getPathwaySize() == 0) {
                return;
            }
            ppd.selectPathway(0);

            new WebDriverWait(getDriver(), 5).until(driver -> !ppd.isOpen());
        }
    }

    @Test(enabled = false, groups = {"clinical.playbook"})
    public void validateSavingsPlay() {
        this.playNames = new ArrayList<>();
        for (int i = 0; i < clinical.getNumberOfVariations(); i++) {
            clinical.selectVariation(i);

            if (clinical.getNumberOfPathways() == 0) {
                return;
            }
            clinical.selectPathway(0);


            if (clinical.getNumberOfAttributedPhysicians() == 0) {
                return;
            }
            clinical.selectAttributedPhysician(0);

            /*Open playbook*/
            var playbook = clinical.getActionBarModal().openPlaybook();

            String format = String.format("%s : %s -> create-play-test[%s]",
                    getClientId(), getAcoId(), (Math.random() * 10000));
            playNames.add(format);
            playbook.setPlayName(format);

            playbook.setDescription("Test Play Description fdskjfsajfds");
            playbook.savePlay();
        }
    }

    @Test(enabled = false, groups = {"playbook.delete"})
    public void deleteCreatedPlays() {
        NavMenu.PB_SAVINGS.navigate(getDriver());
        for (String playName : playNames) {
            EditPlaySummary editPlaySummary = new EditPlaySummary(getDriver());
            EditPlayModal editPlayModal = editPlaySummary.openPlay(playName);
            editPlayModal.deletePlay();
        }
    }
}
