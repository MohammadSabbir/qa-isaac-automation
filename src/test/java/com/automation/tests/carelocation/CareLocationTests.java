package com.automation.tests.carelocation;

import com.api.navigation.NavMenu;
import com.api.pages.CareLocation;
import com.api.pages.EditPlayModal;
import com.api.pages.EditPlaySummary;
import com.automation.tests.base.BaseTest;
import com.google.common.base.Verify;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class CareLocationTests extends BaseTest {

    private CareLocation careLocation;
    private List<String> playNames;

    public CareLocationTests(int clientId, String aco) {
        super(clientId, aco);
    }

    @Test(groups = {"clinical"})
    public void careLocationTests() {
        this.careLocation = new CareLocation(getDriver());
        NavMenu.CARE_LOCATION.navigate(getDriver());
        validateHeader();
        validatePPD();
        validateSavingsPlay();
        deleteCreatedPlays();
    }

    @Test(groups = {"ia.header"}, enabled = false)
    public void validateHeader() {
        boolean headerMatch = this.careLocation.getHeader()
                .equalsIgnoreCase("care location advisor: inpatient");
        boolean logoDisplayed = this.careLocation.clientLogoDisplayed();
        Verify.verify(headerMatch && logoDisplayed,
                String.format("{tc: validateHeader, header-match: %s, advisor: ia}, {tc: validateClientLogo, logo-displayed:%s}"
                        , headerMatch, logoDisplayed));

    }

    @Test(enabled = false, groups = {"ia.ppd"})
    public void validatePPD() {
        if (careLocation.getNumberOfPathways() == 0) {
            return;
        }
        careLocation.selectPathway(0);


        if (careLocation.getNumberOfAttributedPhysicians() == 0) {
            return;
        }
        careLocation.selectAttributedPhysician(0);

        /*Open PPD*/
        var ppd = careLocation.getActionBarModal().openPPD();
        Verify.verify(ppd.isOpen());
        if (ppd.getOpportunitySize() == 0) {
            return;
        }
        ppd.selectOpportunityDetail(0);

        if (ppd.getPathwaySize() == 0) {
            return;
        }
        ppd.selectPathway(0);
        ppd.close();
        new WebDriverWait(getDriver(), 5).until(driver -> !ppd.isOpen());
    }

    @Test(enabled = false, groups = {"ia.playbook"})
    public void validateSavingsPlay() {
        this.playNames = new ArrayList<>();

        if (careLocation.getNumberOfPathways() == 0) {
            return;
        }
        careLocation.selectPathway(0);


        if (careLocation.getNumberOfAttributedPhysicians() == 0) {
            return;
        }
        careLocation.selectAttributedPhysician(0);

        /*Open playbook*/
        var playbook = careLocation.getActionBarModal().openPlaybook();

        String format = String.format("%s : %s -> create-play-test[%s]",
                getClientId(), getAcoId(), (Math.random() * 10000));
        playNames.add(format);
        playbook.setPlayName(format);

        playbook.setDescription("Test Play Description fdskjfsajfds");
        playbook.savePlay();
    }

    @Test(enabled = false, groups = {"playbook.delete"})
    public void deleteCreatedPlays() {
        NavMenu.PB_SAVINGS.navigate(getDriver());
        for (String playName : playNames) {
            EditPlaySummary editPlaySummary = new EditPlaySummary(getDriver());
            EditPlayModal editPlayModal = editPlaySummary.openPlay(playName);
            editPlayModal.deletePlay();
        }
    }
}
