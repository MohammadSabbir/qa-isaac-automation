package com.automation.tests.dashboard;

import com.api.aco.Aco;
import com.api.aco.DashboardAco;
import com.api.pages.Dashboard;
import com.api.util.MathUtils;
import com.automation.tests.base.BaseTest;
import com.google.common.base.Verify;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.stream.Collectors;

/**
 * A Test class for Dashboard Tests.
 */
public class DashboardTests extends BaseTest {

    private Dashboard dashboard;

    public DashboardTests(int clientId) {
        super(clientId, "all");
    }

    @BeforeClass(alwaysRun = true, dependsOnMethods = "login")
    @Override
    public void selectAco() {
        //No need to select ACO
    }

    @Test(groups = {"dashboard"})
    public void dashboardTests() {
        this.dashboard = new Dashboard(getDriver());
        validateHeader();
        validateBene1();
        validateBene0();
        validateAllChicletValue();
        validateChicletsNotClickable();
    }

    @Test(enabled = false, groups = {"dashboard.header"})
    public void validateHeader() {
        boolean headerMatch = this.dashboard.getHeader().equalsIgnoreCase("dashboard");
        boolean logoDisplayed = this.dashboard.clientLogoDisplayed();
        Verify.verify(headerMatch && logoDisplayed,
                String.format("{tc: validateHeader, header-match: %s, advisor: nra}, {tc: validateClientLogo, logo-displayed:%s}"
                        , headerMatch, logoDisplayed));
    }

    @Test(enabled = false, groups = {"dashboard.opportunity"})
    public void validateBene1() {
        List<DashboardAco> levelOneContracts = dashboard.getLvl1s();
        for (DashboardAco levelOneContract : levelOneContracts) {
            if (levelOneContract.hasChildren()) {
                double expectedOpp = levelOneContract.getValue(getDriver());
                double addedTotalOpportunity = levelOneContract.getChildren().stream()
                        .mapToDouble(e -> e.getValue(getDriver())).sum();
                double percentDifference = MathUtils.percent(expectedOpp, addedTotalOpportunity);

                String debugMessage = String.format("{client: %s, aco:%s, totalOpportunity:%s, percent-difference:%s},{aco:%s, totalOpportunity:%s}",
                        getClientId(), getAcoId(), expectedOpp, percentDifference,
                        levelOneContracts.stream().map(Aco::getAco).collect(Collectors.toList()),
                        addedTotalOpportunity);

                Verify.verify(percentDifference < 10, debugMessage);
            }
        }
    }

    @Test(enabled = false, groups = {"dashboard.opportunity"})
    public void validateBene0() {
        List<DashboardAco> level0Contracts = dashboard.getLvl0s();

        for (DashboardAco roots : level0Contracts) {
            if (roots.hasChildren()) {
                double expectedOpp = roots.getValue(getDriver());
                double addedTotalOpportunity = roots.getChildren().stream()
                        .mapToDouble(e -> e.getValue(getDriver())).sum();
                double percentDifference = MathUtils.percent(expectedOpp, addedTotalOpportunity);

                String debugMessage = String.format("{client: %s, aco:%s, totalOpportunity:%s, percent-difference:%s},{aco:%s, totalOpportunity:%s}",
                        getClientId(), getAcoId(), expectedOpp, percentDifference, level0Contracts.stream().map(Aco::getAco).collect(Collectors.toList()),
                        addedTotalOpportunity);

                Verify.verify(percentDifference < 10, debugMessage);
            }
        }
    }

    @Test(groups = {"dashboard.opportunity", "dashboard.chiclet"}, enabled = false)
    private void validateAllChicletValue() {
        //Grab the total opportunity from chiclets
        double totalOppFromChiclet = dashboard.getTotalOppFromChiclet();
        double totalOppFromContract = dashboard.getLvl0s().stream().mapToDouble(e -> e.getValue(getDriver())).sum();
        double percent = MathUtils.percent(totalOppFromChiclet, totalOppFromContract);

        String debugMessage = String.format("{client: %s, aco:%s, chiclet-opp:%s, percent-difference:%s}," +
                                            "{added-opp:%s, aco:%s}",
                getClientId(), "all", totalOppFromChiclet, percent,
                totalOppFromContract,
                dashboard.getLvl0s().stream().map(Aco::getAco).collect(Collectors.toList()));

        Verify.verify(percent < 10, debugMessage);
    }

    @Test(groups = {"dashboard.chiclet"}, enabled = false)
    private void validateChicletsNotClickable() {
        String[] chicletsToCheck = new String[]{"NRA/ReferralOptimizationView", "CLA/InpatientAdvisorView",
        "CVA/CVView", "LA/LeakageView", "LP/LeakageView"};

        for (String chicletId : chicletsToCheck) {
            boolean chicletNotClickable = !dashboard.isChicletClickable(chicletId);
            String debugMessage = String.format("{tc: %s, client: %s, aco: %s, %s: %s}",
                    "ValidateChicletsNotClickable", getClientId(), getAcoId(), chicletId, chicletNotClickable);
            Verify.verify(chicletNotClickable, debugMessage);
        }
    }
}
