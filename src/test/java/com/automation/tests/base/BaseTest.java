package com.automation.tests.base;

import com.api.pages.Dashboard;
import com.api.pages.Homepage;
import com.automation.config.AppConfig;
import com.automation.webdriver.DriverFactory;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

/**
 * Boilerplate code for test cases.
 */
public abstract class BaseTest {
    private final int clientId;
    private final String acoId;
    private final String username;
    private WebDriver driver;


    public BaseTest(int clientId, String acoId) {
        this.clientId = clientId;
        this.acoId = acoId;
        synchronized (this) {
            this.username = AppConfig.APP_SETTINGS.USERNAME_SETTINGS.usernameMap.get(Integer.toString(clientId));
        }
    }

    public WebDriver getDriver() {
        return driver;
    }

    public int getClientId() {
        return clientId;
    }

    public String getAcoId() {
        return acoId;
    }

    @BeforeClass(alwaysRun = true, groups = {"init.driver"})
    public void initDriver() {
        if (null == getDriver()) {
            this.driver = DriverFactory.getDriver();
        }
    }

    @BeforeClass(alwaysRun = true, groups = {"init.homepage"}, dependsOnGroups = {"init.driver"})
    public void goToHomepage() {
        getDriver().get(AppConfig.APP_SETTINGS.ENVIRONMENT_SETTINGS.getUrl());
    }

    @BeforeClass(alwaysRun = true, dependsOnGroups = {"init.*"})
    public void login() {
        Homepage hp = new Homepage(getDriver());
        hp.login(username);
    }

    @BeforeClass(alwaysRun = true, dependsOnMethods = "login")
    public void selectAco() {
        Dashboard dashboard = new Dashboard(getDriver());
        if (dashboard.getAco(getAcoId()).isClickable(getDriver())) {
            dashboard.getAco(getAcoId()).selectAco(getDriver());
            dashboard.load();
            Assert.assertTrue(dashboard.getAco(getAcoId()).isSelected(getDriver()));
        }
    }

    @AfterMethod(alwaysRun = true)
    public void closeDriver() {
        getDriver().quit();
    }
}
