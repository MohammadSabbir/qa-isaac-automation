package com.automation.tests.base;

import org.testng.annotations.Test;

/**
 * Sample test code.
 */
public class DemoTest extends BaseTest {

    public DemoTest(int clientId, String aco) {
        super(clientId, aco);
    }

    @Test
    public void sampleDT() {
        System.out.println("Sample Test! " + getDriver().hashCode());
    }
}
