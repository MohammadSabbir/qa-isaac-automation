package com.automation.tests.base;

import org.testng.annotations.Test;

/**
 * This is another demo test.
 */
public class DemoTestTwo extends BaseTest {

    public DemoTestTwo(int clientId, String aco) {
        super(clientId, aco);
    }

    @Test
    public void sampleDT2() {
        System.out.println(getClientId());
        System.out.println(getAcoId());
        System.out.printf("DP2 - STANDARD %d %d %s%n",
                getDriver().hashCode(), getClientId(), getAcoId());
    }

    @Test(dependsOnMethods = "sampleDT2", alwaysRun = true)
    public void sampleDependentTest() {
        System.out.printf("DP2 - DEPENDENT %d %d %s%n", getDriver().hashCode(), getClientId(), getAcoId());
    }
}
