package com.automation.config;

import com.automation.database.DataSources;
import org.junit.Assert;
import org.testng.annotations.Test;

public class ConfigTest {

    @Test(groups = {"config"})
    public void configTest() {
        Assert.assertNotNull(AppConfig.APP_SETTINGS);
        Assert.assertNotNull(DataSources.getQADatabase());
        Assert.assertNotNull(DataSources.getSitDatabase());
    }
}
