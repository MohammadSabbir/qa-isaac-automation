package com.automation;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import java.util.concurrent.atomic.AtomicInteger;

public class Application {

    public static void main(String[] args) {
        var options = createOptions();

        final Object[][] header = new String[][]{{"Option", "Full Option", "Description"}};
        final Object[][] table = new String[options.getOptions().size()][];

        AtomicInteger rowNumber = new AtomicInteger(0);
        for (Option option : options.getOptions()) {
            table[rowNumber.getAndIncrement()] = new String[]{option.getOpt(), option.getLongOpt(), option.getDescription()};
        }

        System.out.println(System.out.format("%15s %15s %15sx", header[0]));
        for (final Object[] row : table) {
            System.out.format("%15s %15s %15s %n", row);
        }
    }

    private static Options createOptions() {
        // create Options object
        Options options = new Options();

        //Browser Options
        options.addOption("b", "browser", false, "the browser type");
        options.addOption("bh", "browser-headless", false, "use a headless browser");
        options.addOption("co", "chrome-options", false, "set chrome options");
        options.addOption("dp", "download-path", false, "set path where files are downloaded too");

        return options;
    }

    private static void app(String[] args) {
        while (true) {

        }
    }
}
